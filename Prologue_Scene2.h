//Prologue_Scene2.h

#ifndef PROLOGUE_SCENE2_H
#define PROLOGUE_SCENE2_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Prologue_Scene2 : public PlayerChoiceStage
{
public:


	Prologue_Scene2(Player * p);
	~Prologue_Scene2();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};



#endif