//Ch4.h

#ifndef CH4_H
#define CH4_H

#include "Environment.h"
#include "Player.h"

/**
An implementation of the Environment interface
 */

class Ch4 : public Environment
{
public:
	Ch4(Player * p);
	~Ch4();

	int getUID();
};


#endif