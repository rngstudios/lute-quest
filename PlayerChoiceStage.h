//PlayerChoiceStage.h

#ifndef PLAYERCHOICESTAGE_H
#define PLAYERCHOICESTAGE_H

#include "Stage.h"

/**PlayerChoiceStage inherits from Stage Class and acts as an adapter to provide a structure for all the
 implementations of Stage where the user has a specific six options (attack(), flee(), etc.) but where the 
 execution of runStage() is the same. Each child of PlayerChoiceStage must define each of the six player
 options (methods). 
 */
class PlayerChoiceStage : public Stage 
{
public:
///Pure virtual destructor to ensure each child individually handles deletion of objects created
	virtual ~PlayerChoiceStage() = 0;

///runStage is a structured execution of Stages of type playerChoiceStage
	int runStage();

///Function to define what should happen when the user command equals "ATTACK"
	virtual void attack() = 0;

///Function to define what should happen when the user command equals "FLEE"
	virtual void flee() = 0;

///Function to define what should happen when the user command equals "INTERACT"
	virtual void interact() = 0;

///Function to define what should happen when the user command equals "TALK"
	virtual void talk() = 0;

/**Function to define what should happen when the user command equals "USE". Also opens the Player's inventory
so long as it is not empty, and provides options for each Item in the Player's inventory
*/
	virtual void use() = 0;

///Function to define what should happen when the user command equals "WALK"
	virtual void walk() = 0;
};


#endif