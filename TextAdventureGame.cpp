//TextAdventureGame.cpp

#include "TextAdventureGame.h"
#include "Prologue.h"
#include "Ch1.h"
#include "Ch2.h"
#include "Ch3.h"
#include "Ch4.h"
#include <time.h>
#include <algorithm>



TextAdventureGame::TextAdventureGame()
{
	title = \
	"\n"
"                          _____________________________ \n"
"                                                        \n"
"                                     \e[1mR N G\e[0m             \n"
"                                                        \n"
"                                 S t u d i o s          \n"
"                                     _____              \n"
"                                                        \n"
"                                  ~~presents~~          \n"
"                                                        \n"
"                             _______________________    \n"
"                            |                       |   \n"
"                            |   L U T E             |   \n"
"                            |       Q U E S T  1.1  |   \n"
"                            |                       |   \n"
"                            |           \e[93m@\e[39m           |   \n"
"                            |          \e[93m@@@\e[39m          |   \n"
"                            |          \e[93m@@@\e[39m          |   \n"
"                            |          \e[93m@@@\e[39m          |   \n"
"                            |          \e[93m@@@\e[39m          |   \n"
"                            |           \e[93m@\e[39m           |   \n"
"                            |                       |   \n"
"                            |           \e[93m@\e[39m           |   \n"
"                            |          \e[93m@@@\e[39m          |   \n"
"                            |           \e[93m@\e[39m           |   \n"
"                            |                       |   \n"
"                            |_______________________|   \n"
"                                                        \n"
"                          _____________________________ \n";

	introduction = \
	"Welcome to Lute Quest!\n"
	"In this game, you take control of Thaddeus the gnome to help him save the world\n"
	"and reclaim his lost loot! ...which is actually a lute, so would it be a lute\n"
	"loot? Or a loot lute? ...oh boy, we’re off to a great start.\n";


	Thaddeus = new Player("Thaddeus");
}

TextAdventureGame::~TextAdventureGame()
{
	delete Thaddeus;

	for (Environment* env : envs)
	{
		delete env;
	}
}

std::string TextAdventureGame::getIntro()
{
	return introduction;
}

std::string TextAdventureGame::getTitle()
{
	return title;
}

void TextAdventureGame::play()
{
	std::string playAgain = "YES";
	///Loops as long as user wishes
	while(playAgain == "YES")
	{
		///Prints TextAdventureGame title and introduction only if it is a new game
		if (!isLoadedGame())
		{
			typeText(getTitle(), 90000);
			typeText(getIntro(), 25000);
			Thaddeus->userContinue();
		}

		/*
		 * Populate if empty
		 */
		if (envs.size() == 0) {
			envs.push_back(new Prologue(Thaddeus));
			envs.push_back(new Ch1(Thaddeus));
			envs.push_back(new Ch2(Thaddeus));
			envs.push_back(new Ch3(Thaddeus));
			envs.push_back(new Ch4(Thaddeus));
		}

		/*
		 * Run through the environments
		 */

		for (unsigned int i = 0; i < envs.size(); i++) {
			activeEnvironment = envs.at(i);

			if (i == 0)
			{
				activeEnvironment->setActiveStage(stageIndex);
				activeEnvironment->runEnvironment(stageIndex);
			} else {
				activeEnvironment->runEnvironment(0);
			}
			if (activeEnvironment->getActiveStage()->isGameSaved()) {
				return;
			}
		}

		envs.clear();



		gameBeaten();
		std::cout << "Would you like to play again? (YES/NO)" << std::endl;
		std::getline(std::cin, playAgain);
		playAgain = convertToUpper(playAgain);

		if(playAgain=="YES")
		{
			delete Thaddeus;
			Thaddeus = new Player("Thaddeus");
		}
	}

}

std::string TextAdventureGame::convertToUpper(std::string s)
{
	for (unsigned int i = 0; i < s.size(); i++)
	{
		std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	}
	return s;
}


void TextAdventureGame::printText(std::string s)
{
	std::cout << s << std::endl;

}

void TextAdventureGame::addEnv(Environment* env) {
	envs.push_back(env);
}

void TextAdventureGame::typeText(std::string s, unsigned int speed)
{
	unsigned int i = 0;
	while(i<s.length())
	{
		if(s.at(i)=='*')
		{
			std::cout << s.at(i) << std::flush;
			usleep(300000);
		}
		else if((s.at(i) =='.') || (s.at(i)=='!') || (s.at(i)=='?'))
		{
			std::cout << s.at(i) << std::flush;
			usleep(300000);
		}
		else if(s.at(i)==',')
		{
			std::cout << s.at(i) << std::flush;
			usleep(200000);
		}
		else if((s.at(i)==' ') || (s.at(i)=='|') || (s.at(i)=='_'))
		{
			std::cout << s.at(i) << std::flush;
		}
		else
		{
			usleep(speed);
			std::cout << s.at(i) << std::flush;
		}
		i++;
	}
	usleep(s.length()*100);
	std::cout << std::endl;
}


bool TextAdventureGame::isOver()
{
	return false;
}

std::string TextAdventureGame::getTime(clock_t start, clock_t end)
{
	int diff = (end - start);
	int seconds = diff % 60;
	int minutes = ((diff/60) % 60);
	int hours = ((diff/60/60) % 24);
	std::string t, s, m, h;
	s = std::to_string(seconds);
	m = std::to_string(minutes);
	h = std::to_string(hours);
	t = "You have been playing for " + h + " hours, " + m + " minutes, and " + s + " seconds.\n";
	return t;
}

Player* TextAdventureGame::getPlayer() {
	return Thaddeus;
}

Environment* TextAdventureGame::getActiveEnv() {
	return activeEnvironment;
}

void TextAdventureGame::clearScreen()
{
	std::string clearScreen;
	for(int i = 0; i < 60; i++)
	{
		clearScreen = clearScreen + "\n";
	}
	printText(clearScreen);
}

void TextAdventureGame::setEnvironment(Environment* env) {
	activeEnvironment = env;
}

void TextAdventureGame::setPlayer(Player* player) {
	Thaddeus = player;
}

std::vector<Environment*> TextAdventureGame::getEnvs() {
	return envs;
}

void TextAdventureGame::setStageIndex(const int val) {
	stageIndex = val;
}

void TextAdventureGame::setLoadedGame(const bool val) {
	loadedGame = val;
}

bool TextAdventureGame::isLoadedGame() {
	return loadedGame;
}

void TextAdventureGame::gameBeaten()
{
	usleep(5000000);
	clearScreen();
	std::string s = \
	" ______________________________________________________________________________ \n"
	"|                                                                              |\n"
	"|    @@@     @@@  @@@@@@@@    @@@   @@@                                        |\n"
	"|     @@@   @@@  @@@    @@@   @@@   @@@                                        |\n"
	"|      @@@ @@@   @@@    @@@   @@@   @@@                                        |\n"
	"|        @@@     @@@    @@@   @@@   @@@                                        |\n"
	"|        @@@     @@@    @@@   @@@   @@@                                        |\n"
	"|        @@@     @@@    @@@   @@@   @@@                                        |\n"
	"|        @@@      @@@@@@@@     @@@@@@@                                         |\n"
	"|                                                                              |\n"
	"|                    @@@              @@@  @@@@@@@@@  @@@@@@     @@@   @@@ @@@ |\n"
	"|                    @@@              @@@     @@@     @@@ @@@    @@@   @@@ @@@ |\n"
	"|                     @@@    @@@@    @@@      @@@     @@@  @@@   @@@   @@@ @@@ |\n"
	"|                      @@@  @@  @@  @@@       @@@     @@@   @@@  @@@   @@@ @@@ |\n"
	"|                       @@@@@    @@@@@        @@@     @@@    @@@ @@@           |\n"
	"|                        @@@      @@@      @@@@@@@@@  @@@     @@@@@@   @@@ @@@ |\n"
	"|______________________________________________________________________________|\n";
	typeText(s, 50000);
	usleep(5000000);
	clearScreen();
}
