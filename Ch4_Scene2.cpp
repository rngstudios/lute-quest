//Ch4_Scene2.cpp
#include "Ch4_Scene2.h"

using namespace std;


	Ch4_Scene2::Ch4_Scene2(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 4: Scene 2\n";
		introduction = \
		"\n"
		"With a click, you push the small button in the wall.\n"
		"Suddenly, you hear a ticking noise.  A very mysterious ticking noise.  You wait,\n"
		"hoping that something horrible doesn’t happen.  You find the ticking to actually\n"
		"be kind of catchy and start humming along with it.\n"
		"Just as you’re about to leave for fear of the wall exploding or some other\n"
		"horrible catastrophe, a metal can swings down from above and smacks you in the\n"
		"head.\n"
		"Looking up, you see a stout skeleton wearing a beret holding another metal can.\n"
		"It is then that you notice that the two are connected by a string.  The skeleton\n"
		"is gesturing for you to hold the can up to your ear.\n"
		"As soon as you do, he shouts into his can in a high-pitched voice, “GO AWAY!\n"
		"THIS IS A PERFECTLY LEGAL ENTERPRISE AND WE’RE NOT EXPECTING COMPANY!”\n";
	}

	Ch4_Scene2::~Ch4_Scene2(){}
	
	void Ch4_Scene2::attack()
	{
		string s = \
		"\n"
		"“Well, that’s too bad, because you’ve got some company!” you yell at the\n"
		"skeleton.\n"
		"You wind up and hurl the can right at his head.  The can makes contact and the\n"
		"skeleton’s head goes sailing away.\n"
		"As it does, however, it calls out, “INTRUDER ALERT!!!  INTRUDER ALERT!!!\n"
		"MAIN ENTRANCE!!!”\n"
		"The stone wall suddenly swings open and an army of skeletons come pouring out,\n"
		"yelling ferocious battle cries.  Their shouts fade away as they realize there’s\n"
		"no intruder in sight.\n"
		"Disappointed at the lack of a fight, they turn around and slowly march back into\n"
		"the fortress.  They close the door behind them, unaware of the incredibly flat,\n"
		"vaguely gnome-shaped decoration that now adorns the outside.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch4_Scene2::flee()
	{
		string s = \
		"\n"
		"The sight of a talking skeleton makes you panic, and you begin to run down the\n"
		"slope.\n"
		"However, you forgot to let go of the can, and the skeleton on top of the wall\n"
		"loses his balance and falls down on top of you.  The two of you get entangled\n"
		"like a couple of cartoon characters and roll down the hill, flying off the ridge\n"
		"at the bottom to your imminent demise.  At least for you.  The skeleton might\n"
		"survive.\n"
		"Too bad you’re not playing as him, hey?\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch4_Scene2::interact()
	{
		string s = \
		"\n"
		"You decide that you need some time to consider your next course of action and\n"
		"try to put the skeleton on hold.\n"
		"Then you remember that you’re using a tin can phone.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = false;
	}

	void Ch4_Scene2::talk()
	{
		string s = \
		"\n"
		"“Any chance that I could just come in anyway?” you ask the skeleton.  “I’m a\n"
		"friend of Koops’.”\n"
		"“Ah, a classic excuse!” chuckles the skeleton.  “What you don’t know is that our\n"
		"master doesn’t have any friends!”\n"
		"“Oh...fair enough, I suppose.”\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = false;
	}

	void Ch4_Scene2::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			protagonist->removeItem("MATCHES", 1);
			string s = \
			"\n"
			"You light a match and stare at it for a while.  Then you realize that there’s\n"
			"really nothing that you can do with it at the moment and throw it to the ground.\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			protagonist->removeItem("CRYSTAL BALL", 1);
			string s = \
			"\n"
			"You pull out the crystal ball and hurl it at the skeleton.\n"
			"However, you underestimate how heavy the ball is, and it instead hits the\n"
			"wall in front of you and shatters, raining crystal shards down on top of\n"
			"you.\n"
			"You shield your head as the skeleton cackles at your failed assault.\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			protagonist->removeItem("PIXIE-B-GONE", 1);
			string s = \
			"\n"
			"You spray the contents of the Pixie-B-Gone canister at the skeleton.\n"
			"“Ah, ah, ah, ACHOO!” The skeleton lets out a might sneeze that sends it\n"
			"tumbling back a short distance.\n"
			"“Wait, how did you sneeze?  You don’t even have a nose!” you call up.\n"
			"“I *sniffle* I don’t knows.  Heheh Knows nose...” replies the skeleton.\n"
			"You drop the empty can on the ground and consider if there is a way to avoid\n"
			"more horrible puns.\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "BERRY")
		{
			protagonist->removeItem("BERRY", 1);
			string s =\
			"\n"
			"You pull out a berry and chew it thoughtfully.\n"
			"“Ooo, can I have one?” asks the skeleton.\n"
			"“Not unless you let me in.”\n"
			"“Well, I couldn’t taste it anyway, so forget it!  Nice try!”\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if (userChoice == "MOUNTAIN MEAD BOTTLE")
		{
			string s = \
			"\n"
			"You hold up the bottle of Mountain Mead to the skeleton.\n"
			"“But I come bearing a gift!”\n"
			"“Ooo, is that Mountain Mead?” asks the skeleton.\n"
			"“You bet it is!  Only the best for Master Koops!” you call back.\n"
			"“Well, in that case, come on in!  Take a step back so I can open the\n"
			"door, and I’ll send for an escort for you!”\n";
			typeText(s, 25000);
			stageValue = 3;
			completed = true;
		}
	}

	void Ch4_Scene2::walk()
	{
		string s = \
		"\n"
		"You pace around in circles, hoping that maybe if you loiter long enough, the\n"
		"skeleton will let you in.\n"
		"However, when you look up he’s staring up at the sky with a finger through his\n"
		"nose.\n"
		"It’s clear that he’s already forgotten about you.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = false;
	}
	