//Ch4_Scene3.cpp
#include "Ch4_Scene3.h"

using namespace std;


	Ch4_Scene3::Ch4_Scene3(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 4: Scene 3\n";
		noCount = 0;
		introduction = \
		"\n"
		"As the door to Koops’ fortress swings open before you, you are in awe of the\n"
		"cavernous interior.  Like, sure, you knew it was in the side of a mountain, so\n"
		"it would be cavernous, i.e. a cave, but...you know what, never mind.\n"
		"Anyway, as you stare in awe, a tall gangly skeleton wearing a floppy purple\n"
		"wizard hat strolls up to you.\n"
		"“Well, howdly doodly, traveller!” he exclaims.\n"
		"“You’re certainly cheerful,” you give him a questioning look, then stop.\n"
		"“Wait, are you Koops?”\n"
		"“Nosiree!  I’m just a lowly skeleton servant, sir!  Koops is somewhere in this\n"
		"neat-o fortress!”\n"
		"“Oh, okay.  Umm, well, I brought this donation for his treasure pile,” you say,\n"
		"holding up the bottle of Mountain Mead, “so is there somewhere I can take it?”\n"
		"“Boy-oh-boy, a gift?  Well, that’s just dandy!  We’ll just waltz over to the\n"
		"treasure pile and give it a new home there!  It’s just on the other side of the\n"
		"room!”\n"
		"Then you see a massive golden pile at the end of the room.  In fact, you don’t\n"
		"know how you missed it when you first walked in.  As the skeleton leads you over\n"
		"to the pile, you tune out his talk of how Koops “canoodled all these valuables\n"
		"from the neighborinos” and instead examine the room.\n"
		"The hall is massive, but plain.  All it consists of is one long stone walkway,\n"
		"leading to the back of the room, with a platform at the end covered with\n"
		"treasure.  Looking down on either side of the bridge, you can see that the\n"
		"ground beneath the walkway is ablaze – the fire rising up the walls as though it\n"
		"was a caged animal trying to escape.\n"
		"As you reach the end of the walkway and approach the shimmering pile of\n"
		"treasure, the skeleton’s skull starts shaking and his eyes start flashing.\n"
		"“Whoa, are you okay?” you ask.\n"
		"“Yeppers!  I just have to go to a stafferino meeting!  I’ll be back in a jiffy!”\n"
		"explains the skeleton before dashing back down the walkway, leaving you all\n"
		"alone.\n"
		"You stare up at the treasure pile – it looks even bigger up close.\n"
		"You can see everything from gold coins to literal silverware to platinum ingots\n"
		"and even some diamonds.  Then, as you look closer, you see some knobs and string\n"
		"sticking out of an ornate bronze bowl.\n"
		"Could it be?\n"
		"It is!\n"
		"Your golden lute!  At long last, you’ve found it!  It’s just a bit out of your\n"
		"reach, though, and looks like it might be a bit stuck in the treasure pile.\n"
		"Do you pull it out?\n\n";
	}

	Ch4_Scene3::~Ch4_Scene3(){}
	
	void Ch4_Scene3::getHelp()
	{
		string s = \
		"Enter 1 to select YES.\n"
		"Enter 0 to select NO.\n"
		"Enter SAVE to save your game and quit.\n";
		typeText(s, 50000);
	}

	void Ch4_Scene3::choice_1()
	{
		string s = \
		"\n"
		"It’s finally there, right in front of you!\n"
		"You lay the bottle of Mountain Mead down at the base of the pile (may as well\n"
		"leave something), and stretch out your arm to reach for the lute’s handle.\n"
		"As your hand closes around the lute, you carefully pull it out of the pile,\n"
		"wiggling it gently as you do.  A few coins slide down, but everything’s good so\n"
		"far.  Finally, it’s just about out of the pile, so you take a deep breath, hold\n"
		"it, and pull the lute out.\n"
		"**Silence**\n"
		"\n-----------------------------*Got the Golden Lute!*-----------------------------\n"
		"\nYou let out a sigh of relief.  And then you see it.\n"
		"The massive bronze bowl that the lute was in is tilting gently on a precarious\n"
		"pile of treasure.  You were so focused on pulling out your lute that you didn’t\n"
		"realize that all the gold sliding away was support for the bowl!  About the time\n"
		"that you see this, your “sigh of relief” from a few lines back hits the side of\n"
		"the bowl.  The bowl leans back, swings forward, and begins to fall.\n"
		"What follows can only be described as an orchestra of destruction.\n"
		"The bowl smashes into a gold bowl, letting out a colossal *CLANG*.  Then it\n"
		"continues to fall, smashing into seemingly every item on the pile, *CRASH*ing,\n"
		"*BANG*ing, and *CLANG*ing its way all the way down.\n"
		"Then, the entire pile, now unstable, collapses, with gold coins ringing out as\n"
		"the run along the stone walkway, falling into the abyss.  Pots, pans, ingots,\n"
		"and even Jensen’s (the town mayor) kitchen sink (made completely of diamonds)\n"
		"come crashing down onto the walkway, to the point where you think you’ll go\n"
		"deaf.  Every time you think the cacophony is about to die down, it seems to pick\n"
		"up in greater strength.\n"
		"Finally, the pile settles – half of it on the platform and half of it in the pit\n"
		"of fire below.\n"
		"“You know, I knew I should have split that into separate piles.  It was starting\n"
		"to look awfully precarious,” says a voice behind you.\n";
		protagonist->removeItem("MOUNTAIN MEAD BOTTLE", 1);
		protagonist->addItem("Golden Lute", "The legendary instrument that you've been searching for!", 1);
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}

	void Ch4_Scene3::choice_0()
	{
		string s = \
		"\n";

		switch(noCount)
		{
			case 0:
			{
				s += \
				"You decide that no, it’s too risky.  The lute might end up hitting the bowl as\n"
				"you pull it out, and either way it’s going to make the pile resettle, creating a\n"
				"lot of noise.  You’d probably get caught.\n"
				"But then you remember that this is the entire reason you set out on your quest.\n"
				"Maybe you should reconsider...\n";
				typeText(s, 25000);
				noCount++;
				stageValue = 3;
				completed = false;
				break;
			}
			case 1:
			{
				s+= \
				"No, bad idea.  Definitely.\n"
				"All that fire when you came in?  It’s got your name on it.\n"
				"And this place is deathly quiet.  All it would take is for a couple of coins to\n"
				"get out of place to cause a commotion that would bring an army of skeletons down\n"
				"on you.\n"
				"But it IS your prized family heirloom...";
				typeText(s, 25000);
				noCount++;
				stageValue = 3;
				completed = false;
				break;
			}
			case 2:
			{
				s+= \
				"Okay, you know what?  Fine.  I tried so hard to give you the ability to choose\n"
				"yes of your own free will, and you still didn’t.\n"
				"This is what your quest is all about!  This is what’s been on Thaddeus’ mind for\n"
				"all of...well...a day, but still!  It’s important!\n"
				"You can’t beat the game if you don’t get the lute back!  What, you want to play\n"
				"this game for the rest of your life?  It’s so great that you don’t want it to\n"
				"end?  I mean, I appreciate that (no really, I’m blushing), but seriously!\n"
				"I have other things I need to do!  I don’t have time to write an entirely new \n"
				"28-page script centring on Thaddeus as he ventures back to the village and\n"
				"decides to get his prized family heirloom back sometime when it’s not buried\n"
				"under a noisy pile of treasure!\n"
				"You’ve ventured through a massive forest, climbed one of the tallest mountains\n"
				"around, been flipped off by a goat, probably died countless times along the way,\n"
				"and now you’re going to let a little bit of clattering gold stop you?\n"
				"You know what, no.\n"
				"Not on my watch.\n"
				"You know those games where someone asks you to do something and if you say no,\n"
				"they just keep asking you until you finally say yes?  I’ve never been a fan of\n"
				"those, so I’m not going to do that.  I’m just going to make the choice for you!\n"
				"You’ve made so many choices over the course of this game, forcing me to write\n"
				"different outcomes for every single one!\n"
				"You know what?  This time it’s my turn!  I’m calling the shots!\n"
				"I AM ASSUMING DIRECT CONTROL OF THE SITUATION.\n";
				typeText(s, 25000);
				choice_1();
				break;
			}
		}
	}
	