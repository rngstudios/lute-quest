//BinaryChoiceStage.cpp

#include "BinaryChoiceStage.h"

BinaryChoiceStage::~BinaryChoiceStage(){}

int BinaryChoiceStage::runStage()
{
	stageValue = -1;
	while(stageValue == -1)
	{
		typeText(title, 100000);

		if (firstRun)
		{
			/////SET THIS TIME TO 25000 WHEN NOT TESTING!!!!!!!!!!!!!!!!!
			typeText(getIntro(), 25000);
		}
		else
		{
			std::cout << getIntro() << std::endl;
		}

		getHelp();
		while (!completed)
		{
			textBreak();
			std::string command = protagonist->getUserCommand();
			while(command == "HELP")
			{
				getHelp();
				command = protagonist->getUserCommand();
			}
			if(command == "1")
			{
				choice_1();
			}
			else if(command == "0")
			{
				choice_0();
			}
			else if (command == "SAVE")
			{
				std::cout << "Saving Game!" << std::endl;
				gameSaved = true;
				return 1337;
			}		
			else
			{
				std::cout << "Error: Invalid command.  Please type HELP if you want a list of the commands."<<std::endl;
			}
		}	
		if(stageValue == -1)
		{
			gameOver();
			completed = false;
			stageValue = -1;
			firstRun = false;
		}
	}
	
	return stageValue - 1;
}