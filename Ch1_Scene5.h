//Ch1_Scene5.h

#ifndef CH1_SCENE5_H
#define CH1_SCENE5_H

#include "Player.h"
#include "NoChoiceStage.h"

class Ch1_Scene5 : public NoChoiceStage
{
public:


	Ch1_Scene5(Player * p);
	~Ch1_Scene5();
	bool chapterDone;
private:
	Player * protagonist;
};
#endif
