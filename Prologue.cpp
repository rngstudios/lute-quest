//Prologue.cpp

#include "Prologue.h"
#include "Prologue_Scene1.h"
#include "Prologue_Scene2.h"
#include "Prologue_Scene3.h"
#include "Prologue_Scene4.h"

Prologue::Prologue(Player * p)
{
	title =\
	"\n                      ~~Prologue: A Visitor in the Night~~                              \n";

	stages.push_back(new Prologue_Scene1(p));
	stages.push_back(new Prologue_Scene2(p));
	stages.push_back(new Prologue_Scene3(p));
	stages.push_back(new Prologue_Scene4(p));
}

Prologue::~Prologue()
{
	for(unsigned int i = 0; i < stages.size(); i++)
	{
		delete stages[i];
	}
}


int Prologue::getUID() {
	return 0;
}

