//Ch2.h

#ifndef CH2_H
#define CH2_H

#include "Environment.h"
#include "Player.h"

/**
An implementation of the Environment interface
 */

class Ch2 : public Environment
{
public:
	Ch2(Player * p);
	~Ch2();

	int getUID();
};


#endif