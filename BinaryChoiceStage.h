//BinaryChoiceStage.h

#ifndef BINARYCHOICESTAGE_H
#define BINARYCHOICESTAGE_H

#include "Stage.h"

/**BinaryChoiceStage inherits from Stage Class and acts as an adapter to provide a structure for all the
 implementations of Stage where the user has two options(EAST/WEST, YES/NO, etc.) but where the 
 execution of runStage() is the same. Each child of BinaryChoiceStage must define both of the player's
 options (methods choice_1() and choice_0()). 
 */

class BinaryChoiceStage : public Stage
{
public:	
///Pure virtual destructor to ensure each child individually handles deletion of objects created	
	virtual ~BinaryChoiceStage() = 0;

///runStage is a structured execution of Stages of type BinaryChoiceStage
	int runStage();

///Function to define what should happen when the user command equals "1"
	virtual void choice_1() = 0;

///Function to define what should happen when the user command equals "0"
	virtual void choice_0() = 0;

/**Function to define specifc help menu for the two option in the BinaryChoiceStage. Is virtual since the
two options may differ between BinaryChoiceStages (i.e. East/West or Yes/No)
*/
	virtual void getHelp() = 0;

};


#endif