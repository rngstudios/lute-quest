//PlayerChoiceStage.cpp

#include "PlayerChoiceStage.h"
#include <string>

PlayerChoiceStage::~PlayerChoiceStage() {}

int PlayerChoiceStage::runStage()
{
	stageValue = -1;
	while(stageValue == -1)
	{
		typeText(title, 100000);

		if (firstRun)
		{
			/////SET THIS TIME TO 25000 WHEN NOT TESTING!!!!!!!!!!!!!!!!!
			typeText(getIntro(), 25000);
		}
		else
		{
			std::cout << getIntro() << std::endl;
		}
		
		if(stageValue == 0)
		{
			typeText("\n\nType HELP to see your available commands.\n", 100000);
		}
		while (!completed)
		{
			textBreak();
			std::string command = protagonist->getUserCommand();
			while(command == "HELP")
			{
				command = protagonist->getHelp();
			}
			if(command == "ATTACK")
			{
				attack();
			}
			else if(command == "FLEE")
			{
				flee();
			}
			else if (command == "INTERACT")
			{
				interact();
			}
			else if (command == "TALK")
			{
				talk();
			}
			else if (command == "USE")
			{
				use();
			}
			else if (command == "WALK")
			{
				walk();
			}
			else if (command == "SAVE")
			{
				std::cout << "Saving Game!" << std::endl;
				gameSaved = true;
				return 1337;
			}
			else
			{
				std::cout << "Error: Invalid command.  Please type HELP if you want a list of the commands."<<std::endl;
			}
		}

		if(stageValue == -1)
		{
			gameOver();
			completed = false;
			stageValue = -1;
			firstRun = false;
		}
	}
	
	return stageValue - 1;
}

