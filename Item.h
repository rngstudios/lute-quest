//Item.h

#ifndef ITEM_H
#define ITEM_H

#include <string>


class Item {
protected:
///Unique name of an Item
	std::string name;

///Unique description of an Item
	std::string description;

public:
/**Quantity corresponding to the theoretical number of Items that exist. Quantity reduces the actual
number of Items in existence and thus reduces memory allocated during gameplay 
*/
	int quantity;

///Getter function for private method 'name'
	std::string getName();

///Getter function for private member description
	std::string getDesc();

///Constructor requires that all field values be specificied at time of creation
	Item(std::string n, std::string d, int q);

///Destructor
	~Item();
};



#endif