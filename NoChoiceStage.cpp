
#include "NoChoiceStage.h"


NoChoiceStage::~NoChoiceStage() {}

int NoChoiceStage::runStage()
{
	typeText(title, 100000);
	typeText(getIntro(), 25000);
	completed = true;
	return stageValue -1;
}