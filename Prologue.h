//Prologue.h

#ifndef PROLOGUE_H
#define PROLOGUE_H

#include "Environment.h"
#include "Player.h"

/**
An implementation of the Environment interface
 */

class Prologue : public Environment
{
private:
	
public:
	Prologue(Player * p);
	~Prologue();

	int getUID();
};


#endif