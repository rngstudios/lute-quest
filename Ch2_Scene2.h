//Ch2_Scene2.h

#ifndef CH2_SCENE2_H
#define CH2_SCENE2_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch2_Scene2 : public PlayerChoiceStage
{
public:


	Ch2_Scene2(Player * p);
	~Ch2_Scene2();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
	int attackNumber = 0;
};
#endif