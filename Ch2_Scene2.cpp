//Ch2_Scene2.cpp
#include "Ch2_Scene2.h"

using namespace std;


	Ch2_Scene2::Ch2_Scene2(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 2: Scene 2\n";
		introduction = \
		"\n"
		"You haven’t been walking for long when you suddenly hear a rustling sound to the\n"
		"right.  Unlike the other mysterious noises you’ve heard, though, this one is\n"
		"actually accompanied by visible movement!  You press your back against a tree\n"
		"and watch closely.  As you look on, a massive brown bear comes sauntering out of\n"
		"the bushes, giving you a curious look.  Or is that a look of hunger?\n";
	}

	Ch2_Scene2::~Ch2_Scene2(){}
	
	void Ch2_Scene2::attack()
	{
		string s = \
		"\n"
		"With a mighty yell, you charge at the bear.  You remember reading that if you\n"
		"make yourself seem big and fearsome to a bear, it’ll get frightened and run\n"
		"away!  Unfortunately, you forget that fearsome isn’t really an appropriate\n"
		"adjective when you’re a three-foot tall gnome.  The bear gives you a confused\n"
		"look and swats you with his paw.  You go flying into a nearby tree and your body\n"
		"crumples to the ground. \n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene2::flee()
	{
		string s = \
		"\n"
		"“A buh...a buh...A BEAR!!!!!” you scream.  You whip around and run away as fast\n"
		"as you can.  Rather, you would, if it weren’t for the tree that was immediately\n"
		"behind you.  Remember?  The one that you were just leaning against?  You smash\n"
		"your face into the trunk and fall down, out like a light.  Needless to say,\n"
		"you’re not going to be going anywhere for a while. \n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene2::interact()
	{
		string s = \
		"\n"
		"Throwing caution to the wind, you walk up to the bear and start to pet it.  Or\n"
		"rather, you would have, except that you realize that you’re now missing a hand.\n"
		"And your arm.  And half of your upper body. \n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene2::talk()
	{
		string s = \
		"\n"
		"“Excuse me, Mr. Bear.  Can I help you?” you call out.\n"
		"“Well, acshooly, that’d be swell!” the bear replies.\n"
		"You twitch.  You didn’t expect the bear to talk back!  Since when do bears talk\n"
		"back?  Then you remember that you’re in a fantasy world and pretty much anything\n"
		"is possible.  You decide it’s probably best to just go with it rather than\n"
		"questioning the sanity of the situation.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Ch2_Scene2::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			string s = \
			"\n"
			"You strike a match and throw it at the bear.  It lands on its fur coat,\n"
			"which is almost immediately set ablaze.  With a howl of agony, the bear\n"
			"charges.  Before you have time to react, the bear slams into you, and you’re\n"
			"simultaneously burnt and crushed to death.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			string s = \
			"\n"
			"You hold up the crystal ball, wind up a throw, and hurl it at the bear.  It\n"
			"hits him in the side, bounces off, and shatters on the ground.  The bear\n"
			"stares at you for a moment with a hurt look in its eyes.  Then that hurt\n"
			"turns to anger, and the bear backs you up against a tree and exacts his\n"
			"revenge. \n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			string s = \
			"\n"
			"You run up to the bear and empty the contents of the Pixie-B-Gone canister\n"
			"into his face.  The bear gives you a confused look.  You then realize that\n"
			"bears and pixies are two very different things.  Pixies have wings and\n"
			"sparkle and fly around and stuff.  Bears are like big, furry sausages.\n"
			"No wings.  No sparkle.  Whoops, really dropped the ball on that one.  Don’t\n"
			"worry, though.  The bear decides to make sure it’s a mistake you don’t make\n"
			"twice. \n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
	}

	void Ch2_Scene2::walk()
	{
		string s = \
		"\n"
		"You decide to just leave the bear in piece and go on your way.  You carefully\n"
		"walk by him, making sure that you don’t provoke him.  He gives you a sad look,\n"
		"almost as though he has something he wants to say, but nothing more.  Passing\n"
		"him, you wave farewell, silently thank every god you know that you didn’t get\n"
		"eaten, and carry on.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}
	