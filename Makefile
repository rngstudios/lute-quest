CC=g++11
CFLAGS=-Wall -g -std=c++11
OBJS= TextAdventureGame.o Item.o FantasyAdventure.o Character.o Player.o Stage.o Environment.o \
GameHandler.o PlayerChoiceStage.o BinaryChoiceStage.o NoChoiceStage.o \
Prologue.o Prologue_Scene1.o Prologue_Scene2.o Prologue_Scene3.o Prologue_Scene4.o \
Ch1.o Ch1_Scene1.o Ch1_Scene2.o Ch1_Scene3.o Ch1_Scene4.o Ch1_Scene5.o \
Ch2.o Ch2_Scene1.o Ch2_Scene2.o Ch2_Scene3.o Ch2_Scene4.o \
Ch3.o Ch3_Scene1.o Ch3_Scene2.o Ch3_Scene3.o Ch3_Scene4.o \
Ch4.o Ch4_Scene1.o Ch4_Scene2.o Ch4_Scene3.o Ch4_Scene4.o

PROGRAM=LuteQuest2720

.PHONY: all
all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^

# default rule for compiling .cc to .o
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< ${INCLUDE}

## generate the prerequistes and append to the desired file
.prereq : $(OBJS:.o=.cpp) $(wildcard *.h) Makefile
	rm -f .prereq
	$(CC) $(CCFLAGS) -MM $(OBJS:.o=.cpp) >> ./.prereq 

     ## include the generated prerequisite file
     include .prereq

.PHONY: clean
clean:
	rm -rf *~ *.o

.PHONY: clean-all
clean-all: clean
	rm -rf $(PROGRAM)
	
run: $(PROGRAM)
	$(PROGRAM)
	
memcheck: $(PROGRAM)
	valgrind --leak-check=yes $(PROGRAM)
