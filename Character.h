//Character.h

#ifndef CHARACTER_H
#define CHARACTER_H

#include <vector>
#include <string>
#include <iostream>

/**Character is a base class for all types of characters. Character provides fields and methods   
  common to both Player (user) type characters and NPC's (Non-Player-Characters). 
 */

class Character
{
public:
///Unique name of a character
	std::string name;

///Unique list of dialogue as type string for each character
	std::vector<std::string> dialogue;

///Pure virtual destructor to ensure each instance of character individually handles deletion of objects created
	virtual ~Character() = 0;

///Print function which outputs a string to the display
	void talk(std::string s);
};



#endif