//Ch4.cpp

#include "Ch4.h"
#include "Ch4_Scene1.h"
#include "Ch4_Scene2.h"
#include "Ch4_Scene3.h"
#include "Ch4_Scene4.h"

Ch4::Ch4(Player * p)
{
	title = \
	"\n                                ~~Chapter 4: You~~                                \n";
	stages.push_back(new Ch4_Scene1(p));
	stages.push_back(new Ch4_Scene2(p));
	stages.push_back(new Ch4_Scene3(p));
	stages.push_back(new Ch4_Scene4(p));
}

Ch4::~Ch4()
{
	for(unsigned int i = 0; i < stages.size(); i++)
	{
		delete stages[i];
	}
}

int Ch4::getUID() {
	return 4;
}

