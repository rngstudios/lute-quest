//Ch1_Scene2.cpp
#include "Ch1_Scene2.h"

using namespace std;

	Ch1_Scene2::Ch1_Scene2(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 1: Scene 2\n";
		introduction = \
		"\n"
		// Phillipe
		"“So, in case you didn’t notice, the village got ransacked last night,” Phillipe\n"
		"explains.  “Around 1 AM an army of skeletons invaded the village, broke into\n"
		"people’s houses, and stole their valuables.  I think it was the work of Koops.”\n"
		// Thaddeus
		"“Koops?” you ask.\n"
		// Phillipe
		"“Yeah, Koops.  The evil wizard that supposedly moved into a fortress atop the\n"
		"Smokey Mountain?  He’s known for summoning armies of undead to loot local\n"
		"communities.  Not everyone believes he exists, though, which is why so many\n"
		"people are fighting each other.  They think that there’s a thief among us.\n"
		"Good thing I know the truth.”  He looks at you smugly.\n"
		// Thaddeus
		"“Well, thanks!  Now at least I know what I’m up against!” you tell Phillipe.\n"
		// Phillipe
		"“Oh?  Why do you say that?”\n"
		// Thaddeus
		"“I need to go to Koops’ fortress to get my belongings back.  Case closed, end of\n"
		"story.”\n"
		// Phillipe
		"“But you can’t fight him!  All you ever do is play music!  No offence.”\n"
		// Thaddeus
		"“Well, I guess I’ll just have to figure something out,” you tell Phillipe.\n"
		// Phillipe
		"“Well what got stolen that’s so important?” Phillipe asks.\n"
		// Thaddeus
		"“My family’s priceless golden lute.”\n"
		// Phillipe
		"“Oh, well what specifically?  Do you know?”\n"
		// Thaddeus
		"“My lute.”\n"
		// Phillipe
		"“Yeah, I know, but what did that loot consist of?”\n"
		// Thaddeus
		"“LUTE.  L-U-T-E.”\n"
		// Phillipe
		"“Oh, right, right.  Bard things and all that jazz.  Get it?  Jazz?  Music puns?”\n"
		"he looks at you, chuckling.\n";
	}

	Ch1_Scene2::~Ch1_Scene2(){}
	
	void Ch1_Scene2::attack()
	{
		string s = \
		"\n"
		// Thaddeus
		"“WHAT THE HELL KIND OF HORRIBLE PUN IS THAT?!?!” you scream at Phillipe.  Before\n"
		"he even has time to apologize, you take a swing at his face.  Unfortunately(?),\n"
		"he was expecting your assault, and dodges out of the way, laughing hysterically.\n"
		"He keeps dodging until you finally manage to cool down.\n"
		// Phillipe
		"“Sorry about that, I couldn’t resist!” Phillipe laughs.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Ch1_Scene2::flee()
	{
		string s = \
		"\n"
		// Thaddeus
		"“YOU KNOW I’M TERRIFIED OF PUNS!” you scream at Phillipe.  You’re not actually\n"
		"terrified of them, but he doesn’t need to know that.  Pushing past him, you\n"
		"charge down the street, kicking up crimson hats as you go.  Phillipe stares on\n"
		"in awe and confusion.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}

	void Ch1_Scene2::interact()
	{
		string s = \
		"\n"
		"You start feeling up Phillipe’s face, praying that maybe he has an off switch or\n"
		"something that will spare you from his horrible sense of humor.  Phillipe seems\n"
		"confused, but just decides to continue talking.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Ch1_Scene2::talk()
	{
		string s = \
		"\n"
		// Thaddeus
		"“That was just...why,” you say, “That pun made me physically ill.”\n"
		// Phillipe
		"“Haha Sorry, bud!  Couldn’t resist the temptation!”\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}
	void Ch1_Scene2::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			string s = \
			"\n"
			"You silently pull out your book of matches, strike one, and place it next to\n"
			"Phillipe’s beard.  As his cries of pain fill the street, you feel no remorse.\n"
			"Serves him right for making such horrid puns.\n"
			"Unfortunately, the gnome police were nearby and witness your savage act.  It’s\n"
			"safe to say that you won’t be free to journey to the Smokey Mountain for a\n"
			"loooong time.";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
	}
	
	void Ch1_Scene2::walk()
	{
		string s = \
		"\n"
		"You attempt to bleach your brain of the last 30 seconds as you walk down the\n"
		"street.  You hear Phillipe’s muffled shouts from behind you, but you ignore him.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}
