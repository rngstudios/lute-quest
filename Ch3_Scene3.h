//Ch3_Scene3.h

#ifndef CH3_SCENE3_H
#define CH3_SCENE3_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch3_Scene3 : public PlayerChoiceStage
{
public:


	Ch3_Scene3(Player * p);
	~Ch3_Scene3();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif