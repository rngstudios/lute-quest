//Prologue_Scene4.

#ifndef PROLOGUE_SCENE4_H
#define PROLOGUE_SCENE4_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Prologue_Scene4 : public PlayerChoiceStage
{
public:


	Prologue_Scene4(Player * p);
	~Prologue_Scene4();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};



#endif