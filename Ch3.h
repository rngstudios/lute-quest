//Ch3.h

#ifndef CH3_H
#define CH3_H

#include "Environment.h"
#include "Player.h"

/**
An implementation of the Environment interface
 */

class Ch3 : public Environment
{
public:
	Ch3(Player * p);
	~Ch3();

	int getUID();
};


#endif