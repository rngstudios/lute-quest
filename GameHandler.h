//GameHandler.h

#ifndef LUTE_QUEST_GAMEHANDLER_H
#define LUTE_QUEST_GAMEHANDLER_H

#include <string>
#include "TextAdventureGame.h"

/**
GameHandler, handles save and load functionality with its own serialization
*/

class GameHandler {
private:
    bool ranOnce = false;

public:

    GameHandler(TextAdventureGame* textGame);
    ~GameHandler();
    TextAdventureGame* game;

    /**
     Writes the TextAdventureGame to file using custom serialization
     @param0 string of file path to save to
     */
    void writeToFile(const std::string fileName);
    
    /**
     Reads the searialized object from file
     @param0 string of the path to file
     Sets TextAdventureGame*
     */
    void readFromFile(const std::string pathToFile);

    /**
     Main Gameloop, all data runs through here.
     Save / Load commands are filtered out for serialization
     Other commands are passted to the TextAdventureGame
     */
    void gameLoop();

    /**
     Validates that a file exists
     Used for validating the existance of save files
     */
    bool doesFileExist(const std::string fileName);

    /**
     Gets the execution directory of the program
     */
    std::string getDir();

    /**
     Finds a substring in the parent string between the begining and end strings
     shiftStart will move the starting point for longer start strings
     */
    std::string grabString(const std::string parent, const std::string start, const std::string end, const int shiftStart);


};

#endif //LUTE_QUEST_GAMEhANDLER_H
