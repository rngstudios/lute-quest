//Prologue_Scene3.cpp

#include "Prologue_Scene3.h"

using namespace std;

	Prologue_Scene3::Prologue_Scene3(Player* p)
		:protagonist(p)
	{
		title = "Prologue: Scene 3\n";
		introduction = \
		"\n"
		"As you step into the living room, you gasp and quickly duck behind a couch.\n"
		"Standing in the centre of the room is an armoured skeleton with glowing eyes!\n"
		"A massive scimitar glints on his bony waist.  As you crouch in fear behind your\n"
		"couch, his eye-glow sweeps your way and pauses for a minute.  Does he see you?\n";
	}

	Prologue_Scene3::~Prologue_Scene3(){}
	
	void Prologue_Scene3::attack()
	{
		string s = \
		"\n"
		"He must have seen you!  Time for an ambush!  You leap out from behind the couch,\n"
		"and with a mighty yell (at least, as mighty as a 3-foot tall gnome can be), you\n"
		"charge at the skeleton.  Then suddenly you’re not moving anymore, but being\n"
		"lifted off the ground!  You have just enough time to look down and mutter, “Oh,\n"
		"I’ve been impaled,” before the world goes black.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Prologue_Scene3::flee()
	{
		string s = \
		"\n"
		"This is crazy.  Way too crazy!  You’re going to die!  You jump up to run back to\n"
		"your room.  What you forgot was that the skeleton was watching your hiding spot.\n"
		"You dash upstairs and hide under the bed, but then you hear the sound.\n"
		"\n"
		"*click clack click clack*\n"
		"\n"
		"And closer...\n"
		"\n"
		"*click clack click cla-*\n"
		"\n"
		"Suddenly, a massive glowing skull appears in front of you.  A bony hand reaches\n"
		"under the bed and drags you out.  Try as you might, you can’t break free from\n"
		"its grip.  The skeleton carries you down the stairs and outside.  You have just\n"
		"enough time to see the village being ransacked by skeletons before you feel a\n"
		"sharp pain from behind and join the army yourself.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Prologue_Scene3::interact()
	{
		string s = \
		"\n"
		"You reach out and hug your soft couch, the faux leather pressing into your face.\n"
		"It may not be a teddy bear, but it’ll do in a pinch.  You pray for the\n"
		"skeleton’s gaze to pass over you.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}

	void Prologue_Scene3::talk()
	{
		string s = \
		"\n"
		"Maybe the skeleton just wants a friendly chat!  You continue thinking that until\n"
		"you realize that a sword penetrating your left lung isn’t really a gesture of\n"
		"friendship.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Prologue_Scene3::use()
	{
		typeText(protagonist->openInventory(), 50000);
		stageValue = 3;
		completed = false;
	}

	void Prologue_Scene3::walk()
	{
		string s = \
		"\n"
		"You stand up and walk towards the skeleton.  Maybe he’ll be scared off by your\n"
		"overwhelming confidence and lack of self-preservation?  Too late you remember\n"
		"that skeletons themselves aren’t really known for being scared off by random\n"
		"warriors, instead fighting until the last breath (as it were).  Unfortunately\n"
		"for you, that last breath comes from your lungs, as the skeleton’s scimitar runs\n"
		"through you like butter.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}