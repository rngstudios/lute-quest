
//Prologue_Scene1.cpp


#include "Prologue_Scene1.h"

using namespace std;

	Prologue_Scene1::Prologue_Scene1(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Prologue: Scene 1\n";
		introduction = \
		"\n"
		"*click clack* *click clack* *click clack*\n"
		"\n"
		"You awake to a silence. You could have sworn that you just heard a strange sound\n"
		"coming from downstairs. Almost like rocks being dropped on each other. Perhaps\n"
		"it was just a dream.\n"
		"\n"
		"*click clack* *click clack*\n"
		"\n"
		"No, you definitely hear it! Someone must be in your house! The sound stops\n"
		"again, so you climb out of bed and listen at your bedroom door. You can hear a\n"
		"rustling noise from downstairs.";
	}

	Prologue_Scene1::~Prologue_Scene1(){}
	
	void Prologue_Scene1::attack()
	{
		string s = \
		"\n"
		"Maybe someone’s waiting on the other side of your door!  To ambush you!\n"
		"You wind up with your right hand as you reach for the doorknob with the other.\n"
		"Suddenly you swing the door open and take your punch.  But nobody’s there!\n"
		"Before you can recognize your mistake, your momentum carries you into the\n"
		"opposite wall, and you smash your face into the wood.\n"
		"After lying on the floor moaning for a minute, you slowly get up and feel your\n"
		"nose.  At least it’s not broken.  Definitely going to be feeling it in the\n"
		"morning, though.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}

	void Prologue_Scene1::flee()
	{
		string s = \
		"\n"
		"Robbers are scary!  They can take whatever they want!\n"
		"You run away from the door and dive back into bed.  After shivering under the\n"
		"covers for what seems like an eternity, you eventually hear the sounds\n"
		"downstairs stop, and you fall back asleep.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene1::interact()
	{
		string s = \
		"\n"
		"You open the door slowly and step out into the hallway.  Thankfully, nobody was\n"
		"waiting outside to ambush you.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}

	void Prologue_Scene1::talk()
	{
		string s = \
		"\n"
		"You attempt to strike up a conversation with your door, but it seems to have a\n"
		"closed personality and doesn’t have anything to say.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}

	void Prologue_Scene1::use()
	{
		typeText(protagonist->openInventory(), 50000);
		stageValue = 1;
		completed = false;
	}

	void Prologue_Scene1::walk()
	{
		string s = \
		"\n"
		"You start walking into the door like a glitchy video game character, as if by\n"
		"sheer willpower you will be able to overcome this most difficult obstacle.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}
