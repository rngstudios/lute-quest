//Ch4_Scene1.cpp
#include "Ch4_Scene1.h"

using namespace std;


	Ch4_Scene1::Ch4_Scene1(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 4: Scene 1\n";
		introduction = \
		"\n"
		"You continue your trek up the mountainside as the last light of the sun\n"
		"disappears.  The dirt path continues on until it finally stops in front of a\n"
		"giant stone wall.\n"
		"A dead end.\n";
	}

	Ch4_Scene1::~Ch4_Scene1(){}
	
	void Ch4_Scene1::attack()
	{
		string s = \
		"\n"
		"“Are you serious?!?!  I came all this way for a dead end?” you angrily assault\n"
		"the wall with a barrage of kicks and punches.  It doesn’t really make you feel\n"
		"better, but at least it let you vent a bit.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}

	void Ch4_Scene1::flee()
	{
		string s = \
		"\n"
		"Stone walls are pretty terrifying.  You turn and run as fast as you can back\n"
		"down the slope.\n"
		"However, you slip on the dirt and go sliding down the path, right back down to\n"
		"the ridge where you saw the goat.  You can guess what happens next.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = false;
	}

	void Ch4_Scene1::interact()
	{
		string s = \
		"\n"
		"“There must be some sort of secret entrance.  The hero always feels up the wall\n"
		"for a few minutes before finding some hidden way of getting in,” you think to\n"
		"yourself.\n"
		"As you caress the wall, suddenly your hand touches a small indentation.\n"
		"“I knew it!  Fantasy clichés win again!” you exclaim triumphantly.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}

	void Ch4_Scene1::talk()
	{
		string s = \
		"\n"
		"“Hey, Mr. or Mrs. Wall?  What should I do?” you ask.\n"
		"The wall (which prefers not to identify as a gender, thank you very much) sits\n"
		"silently, giving no helpful indications whatsoever.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}

	void Ch4_Scene1::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			protagonist->removeItem("MATCHES", 1);
			string s = \
			"\n"
			"You strike a match and hold it up to the wall.\n"
			"However, even with the extra light, you can’t see anything that gives an\n"
			"indication as to how one would go about bypassing the wall.\n";
			typeText(s, 25000);
			stageValue = 1;
			completed = false;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			protagonist->removeItem("CRYSTAL BALL", 1);
			string s = \
			"\n"
			"You pull out the crystal ball and contemplate it.  Maybe it has some\n"
			"significance in all of this?\n"
			"However, you can’t seem to see any obvious hole that it would fit into, so you\n"
			"place it back in your pocket.\n";
			typeText(s, 25000);
			stageValue = 1;
			completed = false;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			protagonist->removeItem("PIXIE-B-GONE", 1);
			string s = \
			"\n"
			"Pulling out the Pixie-B-Gone, you spray the wall.\n"
			"However, the wall looks the same as it did before, just with a bit of extra\n"
			"shimmer to it.  You discard the empty can and try to figure out what you should\n"
			"do.\n";
			typeText(s, 25000);
			stageValue = 1;
			completed = false;
		}
		if(userChoice == "BERRY")
		{
			protagonist->removeItem("BERRY", 1);
			string s =\
			"\n"
			"You pull out a berry and chew it thoughtfully.  It is bitter, but with a\n"
			"surprisingly fruity note that lingers on the tongue.  As you swallow the berry,\n"
			"you continue to consider the wall.\n";
			typeText(s, 25000);
			stageValue = 1;
			completed = false;
		}
		if (userChoice == "MOUNTAIN MEAD BOTTLE")
		{
			string s = \
			"\n"
			"You pull out the mead bottle and attempt to break the wall down with it.\n"
			"However, it just bounces off, almost hitting you in the face.  You decide it’s\n"
			"probably best to try a different approach.\n";
			typeText(s, 25000);
			stageValue = 1;
			completed = false;
		}
	}

	void Ch4_Scene1::walk()
	{
		string s = \
		"\n"
		"You decide that maybe this wall is like that train station in that one moderately\n"
		"popular book series about a bunch of kid wizards.  All you have to do is just walk\n"
		"right through it!\n"
		"You take a step forward and bash your head into the wall.  Not to be deterred, you\n"
		"try a couple more times for good measure.  Still nothing.\n"
		"Well, it was worth a shot.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}
	