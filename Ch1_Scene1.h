//Prologue_Scene1.h

#ifndef CH1_SCENE1_H
#define CH1_SCENE1_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch1_Scene1 : public PlayerChoiceStage
{
public:


	Ch1_Scene1(Player * p);
	~Ch1_Scene1();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};



#endif