//Player.cpp

#include <iostream>
#include <algorithm>
#include <string>
#include "Player.h"

Player::Player(std::string n) : gameComplete{false}
{
	name = n;
}

Player::~Player()
{
	for(unsigned int i = 0; i < inventory.size(); i++)
	{
		delete inventory[i];
	}
}

std::string Player::getUserCommand()
{
	std::cout << "What would you like to do?.\n" << std::endl;
    std::string userCmnd;
    std::getline(std::cin, userCmnd);
    userCmnd = convertToUpper(userCmnd);
	return userCmnd;
}


std::string Player::getHelp()
{
	std::cout << \
	"\nYour options are: \n\n"

	"   -> ATTACK   - Use whatever is handy (or your fists if nothing is) to assault\n                 whatever is currently in front of you.\n\n"
	"   -> FLEE     - Turn tail and run or run past whatever is in front of you,\n                 whichever Thaddeus deems appropriate.\n\n"
	"   -> INTERACT - You attempt to interact with whatever is in front of you.\n\n"
	"   -> TALK     - Talk to whatever/whomever is in front of you.\n\n"
	"   -> USE      - Opens your inventory and allows you to select an item to use.\n\n"
	"   -> WALK     - You walk forwards/onwards/etc.\n\n"
	"   -> SAVE     - Will write out a save file where the program has been executed,\n"
	"                 which is called 'saveFile.lute'.  If this file already exists,\n"
	"                 it will be overwritten.\n\n"
	"   -> HELP     - Opens the help menu.  I guess you already know this!\n\n"


	"What would you like to do?.\n";
	std::string s;
	std::getline(std::cin, s);
	s = convertToUpper(s);
	return s;
}

std::string Player::openInventory()
{
	std::string s;
	if(inventory.size() == 0)
	{
		s = \
		"\nYour inventory is currently empty. There is nothing to use at this time. Try\n"
		"picking another option.\n";
		return s;
	}
	else
	{
		std::cout << "\nYour inventory contains: \n\n";
		for(unsigned int i = 0; i < inventory.size(); i++)
		{
			s = s + \
			inventory[i]->getName() + ":\n" 
			"Quantity: (" + std::to_string(inventory[i]->quantity) + ")\n"
			"Description: \n    "  + inventory[i]->getDesc() + "\n\n";
		}
	}
	return s;
}


std::string Player::convertToUpper(std::string s)
{
	for (unsigned int i = 0; i < s.size(); i++)
	{
		std::transform(s.begin(), s.end(), s.begin(), ::toupper);
	}
	return s;
}

std::string Player::getUserItem()
{
	std::cout << "Enter CLOSE if you do not wish to use anything.\n";
	std::string userChoice;
	std::getline(std::cin, userChoice);
	userChoice = convertToUpper(userChoice);
	return userChoice;
}



void Player::addItem(std::string name, std::string description, int quantity)
{
	Item* newItem = new Item(name, description, quantity);
	inventory.push_back(newItem);
}

bool Player::checkInventory(std::string itemName)
{
	for (unsigned int i = 0; i < inventory.size(); i++)
	{
		std::string item = inventory[i]->getName();
		item = convertToUpper(item);
		itemName = convertToUpper(itemName);
		if (item == itemName)
		{
			return true;
		}
	}
	return false;
}

void Player::removeItem(std::string targetItem, int n)
{
	targetItem = convertToUpper(targetItem);
	for (unsigned int i = 0; i < inventory.size(); i++)
	{
		std::string item(convertToUpper(inventory[i]->getName()));
		if(targetItem == item)
		{
			inventory[i]->quantity = inventory[i]->quantity - n;
			if(inventory[i]->quantity <= 0)
			{
				delete inventory[i];
				inventory.erase(std::remove(inventory.begin(), inventory.end(), inventory[i]), inventory.end());
			}
		}
	}

}


void Player::userContinue()
{
	std::cout << "Press ENTER when you're ready..." <<std::endl;
	std::string s;
	getline(std::cin, s); 
}

void Player::setGameCompleted()
{
	gameComplete = true;
}


bool Player::gameCompleted()
{
	return gameComplete;
}
