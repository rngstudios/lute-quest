//Ch1_Scene4.h

#ifndef CH1_SCENE4_H
#define CH1_SCENE4_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch1_Scene4 : public PlayerChoiceStage
{
public:


	Ch1_Scene4(Player * p);
	~Ch1_Scene4();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif