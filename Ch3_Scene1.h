//Ch3_Scene1.h

#ifndef CH3_SCENE1_H
#define CH3_SCENE1_H

#include "Player.h"
#include "BinaryChoiceStage.h"

class Ch3_Scene1 : public BinaryChoiceStage
{
public:
	Ch3_Scene1(Player * p);
	~Ch3_Scene1();
	bool chapterDone;

	void choice_1();
	void choice_0();
	void getHelp();


private:
	Player * protagonist;
};
#endif