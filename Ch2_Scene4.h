//Ch2_Scene4.h

#ifndef CH2_SCENE4_H
#define CH2_SCENE4_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch2_Scene4 : public PlayerChoiceStage
{
public:


	Ch2_Scene4(Player * p);
	~Ch2_Scene4();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif