//Prologue_Scene1.h

#ifndef PROLOGUE_SCENE1_H
#define PROLOGUE_SCENE1_H

#include "Player.h"
#include "PlayerChoiceStage.h"


class Prologue_Scene1 : public PlayerChoiceStage
{
public:


	Prologue_Scene1(Player * p);
	~Prologue_Scene1();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};



#endif