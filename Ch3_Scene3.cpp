//Ch3_Scene3.cpp
#include "Ch3_Scene3.h"

using namespace std;


	Ch3_Scene3::Ch3_Scene3(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 3: Scene 3\n";
		introduction = \
		"\n"
		"The mountain shakes as the goat begins galloping up the path towards you.\n"
		"Immediately, you begin charging up the mountain as fast as you can.  You see\n"
		"that there’s a sharp left turn coming up ahead, and next to it, a small cave.\n"
		"As you approach it, the goat lets out a mighty roar, and the whole mountain\n"
		"shakes again.  You feel like the place is going to collapse!\n"
		"Suddenly, there is a flurry of activity to your right, and your vision is\n"
		"obscured by black shapes frantically flying around.  Apparently there were bats\n"
		"in the cave and now that they’ve been woken up, they’re attacking you!\n"
		"Through the swirl of bats, you can see that the goat has just come around the\n"
		"corner and he’ll be on you in less than a minute!\n";
	}

	Ch3_Scene3::~Ch3_Scene3(){}
	
	void Ch3_Scene3::attack()
	{
		string s = \
		"\n"
		"You frantically flail around, trying to swat the bats away from you.  You feel\n"
		"your hand brush up against a bat, right before it smashes into the cave next to\n"
		"you.\n"
		"The bats, panicked by your sudden outburst, go flying down the hill while you\n"
		"let out a cry of pain, staring at your fractured, bloody hand.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}

	void Ch3_Scene3::flee()
	{
		string s = \
		"\n"
		"You let out a terrified scream and attempt to run up the mountain.\n"
		"However, with your obscured vision, you end up running straight off the ridge,\n"
		"meeting an untimely demise on the rocks below.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = false;
	}

	void Ch3_Scene3::interact()
	{
		string s = \
		"\n"
		"You try to grab one of the bats, hoping that maybe you can find a way to make\n"
		"them stop flying around.\n"
		"However, the bats are too quick for you, and you keep missing.  By the time you\n"
		"actually grab one, the goat is upon you and has knocked all of you clear off the\n"
		"cliff.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = false;
	}

	void Ch3_Scene3::talk()
	{
		string s = \
		"\n"
		"“Hey guys, maybe we can talk this out!” you say to the bats.  “It’s okay!  You\n"
		"just need to calm down!”\n"
		"Incredibly, it seems to be working!  The bats seem less frenzied and slowly come\n"
		"to rest flapping in front of you.  “See, isn’t that better?” you ask the bats.\n"
		"“WELL, LOOKEE HERE!  GOT OURSELVES A BAT WHISPERER!”\n"
		"Oh right.  You forgot about the goat.  You turn around just in time to see his\n"
		"massive hoof meet your comparatively tiny face.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = false;
	}

	void Ch3_Scene3::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			string s = \
			"\n"
			"You attempt to strike a match, but the bats are so frenzied that you end up\n"
			"slipping and dropping the match.  As you lean down to pick it up, you see that\n"
			"the rocks on the ground are bouncing frantically.  Looking up, you see the\n"
			"goat’s hoof just before it kicks your face in.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = false;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			protagonist->removeItem("CRYSTAL BALL", 1);
			string s = \
			"\n"
			"Palming the crystal ball in your pocket, you reach up and hurl it at the bats.\n"
			"However, it completely misses them.  It flies into the wall of the mountain and\n"
			"shatters into pieces.\n"
			"Suddenly, the bats all scatter and go flying down the mountain.\n"
			"Looking down, you see that the sunlight has caught the shattered fragments of\n"
			"the ball and is casting bright shafts of light all around.\n"
			"“OOOOO, WHAT’S THIS?!?!  A LIGHT SHOW?!?!” bellows the mountain goat.\n";
			typeText(s, 25000);
			stageValue = 4;
			completed = false;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			string s = \
			"\n"
			"You spray the can of Pixie-B-Gone at the bats, but they seem unfazed by it.\n"
			"Well, guess they aren’t pixies.  You proved that, at least.  Future generations\n"
			"will remember the incredible discovery you made right before your demise at the\n"
			"hoofs of a crazed, drunken, goat.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = false;
		}
		if(userChoice == "BERRY")
		{
			string s =\
			"\n"
			"You reach into your pocket and pull out a berry.  The bats don’t seem\n"
			"interested in it in the slightest, so you just eat it.\n"
			"It has a slightly sour flavour to it, but it is tasty.  At least you got to\n"
			"enjoy one last meal before being demolished by a rampaging goat.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = false;
		}
		if (userChoice == "MOUNTAIN MEAD BOTTLE")
		{
			string s = \
			"\n"
			"You lash out around you with the bottle of Mountain Mead, hoping to club the\n"
			"bats away from you.\n"
			"However, the bats are too fast, and manage to dodge the bottle.\n"
			"As you swing around, the bottle collides with the stone wall of the cave,\n"
			"bounces off, and smashes you in the face.  The bottle stays intact, so you\n"
			"don’t get a faceful of glass.\n"
			"However, it knocks you out in one blow, and you stumble drunkenly for a\n"
			"minute before falling backwards off the edge of the mountain.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = false;
		}
	}

	void Ch3_Scene3::walk()
	{
		string s = \
		"\n"
		"You decide that they bats don’t really bother you and start walking up the mountain.\n"
		"However, you must have gotten turned around when they attacked you, because you\n"
		"suddenly hit a dead end.\n"
		"Literally.\n"
		"You walked right into the charging mountain goat.\n"
		"Good job.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = false;
	}
	