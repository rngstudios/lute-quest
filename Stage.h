//Stage.h

#ifndef STAGE_H
#define STAGE_H

#include "Player.h"
#include <time.h>
#include <unistd.h>

/**Stage is an interface that has children which further specify structure of actual
 instances of a Stage. Stage class provides useful methods common to all derived types
 of Stage, while providing flexibility in the execution of a specific type of Stage 
 with the pure virtual runStage() method. 
 */

class Stage
{
public:
///Represents whether or not the stage has been completed
	bool completed;

/// Equals true if it is the first time the stage has been run.  Used for skipping text after game overs.
	bool firstRun = true;

///Player pointer used to pass an instance of the player to each individual stage
	Player * protagonist;

///Title of the specific stage
	std::string title;

///Represents whether the user entered SAVE as their command
	bool gameSaved = false;

///Introduction unique to each instance of Stage
	std::string introduction;

public:

	int stageValue;

///Destructor is virtual and is to be defined by each instance Stage
	virtual ~Stage() = 0;

///runStage is virtual and is to be defined according to the type of Stage	
	virtual int runStage() = 0;

///Getter function which returns private member 'completed'
	bool isCompleted();

///Setter function to set 'completed' equal to false. Used in loading games with partially completed environments	
	void resetStageCompletedValue();

///Getter function to return private member 'introduction'
	std::string getIntro();

///Print function that simply outputs a string with no time delay	
	void printText(std::string s);

/**Print function that prints with a time delay (in microseconds) for each character that is not a type of 
punctuation, no time delay for spaces (' '), and a standard delay for punctuation
*/
	void typeText(std::string s, unsigned int speed);

/**Print function that outputs a line across the terminal. Used to make a visual notification to the user that 
a command should be entered	
*/
	void textBreak();

/**Print function that clears the terminal window by outputting 60 empty lines. Used for increasing clarity
and emphasis of specific outputs
*/
	void clearScreen();

///Outputs Game-Over message and handles usleep() methods to delay the restarting of a game
	void gameOver();

///Setter function to set private member 'gameSaved' which in turn breaks the loop in runStage()
	void setGameSaved(bool val);

///Getter function to return private member 'gameSaved'
	bool isGameSaved();

///Setter function to set private member 'completed'
	void setCompleted(const bool val);
};
#endif
