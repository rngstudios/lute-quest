//Ch1_Scene4.cpp
#include "Ch1_Scene4.h"

using namespace std;

	Ch1_Scene4::Ch1_Scene4(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 1: Scene 4\n";
		introduction = \
		"\n"
		"As you near the exit of the town, you see a dark shape out of the corner of your\n"
		"eye.  Without warning, you are tackled by a massive gnome and fall to the ground\n"
		"under his weight.\n"
		"“OH MY GOSH DID I HEAR THAT YOU’RE GOING TO KOOPS’ CASTLE?!?!” shouts a\n"
		"high-pitched, nasally voice.\n";
	}

	Ch1_Scene4::~Ch1_Scene4(){}
	
	void Ch1_Scene4::attack()
	{
		string s = \
		"\n"
		"Without a moment’s hesitation, you start pummeling your attacker.  You scratch,\n"
		"punch, and kick everywhere you possibly can until he finally gets off you and\n"
		"runs screaming into town.  Satisfied with your handiwork, but fearing\n"
		"repercussions, you run out of town.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Ch1_Scene4::flee()
	{
		string s = \
		"\n"
		"You attempt to run away, but you are completely incapacitated under the incredible\n"
		"weight of the gnome.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch1_Scene4::interact()
	{
		string s = \
		"\n"
		"You push and prod at the mass that is slowly crushing you, but get no response.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch1_Scene4::talk()
	{
		string s = \
		"\n"
		"“Is *gasp* that you, *wheeze* Balesworth?” you manage to choke out.\n"
		"“INDEEEEEEED IT IS!” he excitedly responds.\n"
		"“Could you please *choke* get off me?”\n"
		"“Oh, sure!”\n";
		typeText(s, 25000);
		stageValue = 5;
		completed = true;
	}

	void Ch1_Scene4::use()
	{
		string s = \
		"\n"
		"You attempt to reach for your pocket, but your arms are completely pinned.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch1_Scene4::walk()
	{
		string s = \
		"\n"
		"You realize that you are completely unable to move, and decide there must be\n"
		"some way to get this weight off you before you suffocate.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}
