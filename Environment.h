//Environment.h


#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include <time.h>
#include <unistd.h>
#include <vector>
#include "Stage.h"
#include "Item.h"

/**
 Environment is an interface which provides a structure that holds
 multiple instances of stage specific to the environment which hold 
 dialogue and provide other functionality.
 The environment is executed through the runEnvironment() function.
 */

class Environment
{
protected:
///A list of pointers which point to the specific stages within each environment
	std::vector<Stage*> stages;

///Unique title of the environment 
	std::string title;

///Represents whether or not the environment has been completed
	bool environmentComplete;

public:

///Destructor is pure virtual as each environment may require different memory management
	virtual ~Environment() = 0;

///Checks a specific element within the vector<Stage*> stages and returns that specific stage's 'completed' value
	bool stageComplete(int stageNumber);

///Getter function to return private member 'enviromentComplete'
	bool environmentCompleted();

/**
Function which executes the running of an environment by itterating through the vector<Stage*> stages starting
at a given stageIndex and going to the end of the vector/environment 
*/
	void runEnvironment(int stageIndex);

///Getter function which returns private member 'title'
	void printTitle();

/**
Iterates through the stage vector checking their complete value
when it finds a stage that is not complete, returns the index
*/
	int getStartingStageIndex();

/**
Uses getStartingStageIndex and just grabs the stage using vector::at
*/
	Stage* getActiveStage();

/**
Returns the vector of stages from the environment
*/
	std::vector<Stage*> getStages();

/**
Iterates through stages up until the index provided
and sets each stage completed
*/
	void setActiveStage(const int index);
/**
Virtual function that allows each environment 
to be easily and uniquly identified
*/
	virtual int getUID() = 0;
};


#endif