//Ch4_Scene4.cpp
#include "Ch4_Scene4.h"

using namespace std;


	Ch4_Scene4::Ch4_Scene4(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 4: Scene 4\n";
		introduction = \
		"\n"
		"You turn around slowly, sure of the identity of the figure, but unsure of what\n"
		"to expect.  A mysterious figure clothed in a dark purple robe stands before you,\n"
		"his eyes flashing a bright gold.\n"
		"You stare at the figure.  “You.”\n"
		"“What’s your name, gnome?” the figure asks.\n"
		"“Thaddeus,” you respond instinctively, then cursing the fact that you just told\n"
		"an evil wizard your real name.\n"
		"“Mr. Thaddeus, I’m Koops,” responds the figure.  “I see that you wanted to get\n"
		"something of yours back,” Koops gestures to the lute in your hand, “and yet you\n"
		"couldn’t even be bothered to leave something useful in exchange.” He stares\n"
		"disapprovingly at the empty bottle of Mountain Mead.\n"
		"“To be honest, I’m surprised that your skeletons didn’t notice,” you respond.\n"
		"“Well, you know, they don’t exactly have the best vision,” says Koops, rolling\n"
		"his eyes.  “So, was getting caught part of your plan?”\n"
		"“Surprisingly, no.  But since you’re here, tell me: Why do you do it?  Why do\n"
		"you loot the local villages and hoard this treasure?”\n"
		"Koops pauses for a minute, then lifts up his hood.  Long ebony hair spills out,\n"
		"and a pair of piercing gold eyes looks up at you.  A strange mask covers Koops’\n"
		"mouth and nose, but there’s no mistaking it.  The smooth skin.  The high\n"
		"cheekbones.  Koops is actually a woman!\n"
		"You take a step back.\n"
		"“What?  You’re a girl?” you exclaim.\n"
		"“Um, yeah?  What, girls can’t be nefarious wizards?”\n"
		"“Well, I guess they can...Fine, whatever.  This isn’t really the craziest thing\n"
		"I’ve seen all day.  So tell me: Why do you pillage the towns?”\n"
		"Koops gets a wistful look in her eye.\n"
		"“I wasn’t always this way,” she tells you.  “Once I was an ordinary girl\n"
		"learning magic, completely indistinguishable from everyone else.  No one cared\n"
		"who I was till I put on the mask.”\n"
		"“So what made you do it?  If you take it off, will you die?” you ask.\n"
		"“It would be extremely painful.  But exactly what would happen is really none of\n"
		"your concern.  You don’t get to know my life story.  You don’t get to know\n"
		"anything about me except for the fact that I am the one who will kill you,”\n"
		"explains Koops as matter-of-factly as if she was talking about the weather.\n"
		"She leans forward and thrusts an arm out to either side.  A blast of green\n"
		"energy emanates from either palm and slams into the sides of the room, bathing\n"
		"the entire hall in a green glow.  The light intensifies quickly, until it\n"
		"becomes so blinding you can’t see a thing.  Then, without warning, it\n"
		"disappears, and behind Koops is an army of skeletons stretching from the main\n"
		"entrance all the way to where she is standing – about halfway across the\n"
		"walkway.\n"
		"“GO, MY ARMY!” shouts Koops, “AND THROW THIS INTRUDER INTO THE FIRE PIT WHERE HE\n"
		"BELONGS!”\n";
	}

	Ch4_Scene4::~Ch4_Scene4(){}
	
	void Ch4_Scene4::attack()
	{
		string s = \
		"\n"
		"Koops is too far away for you to run up and attack – by the time you got there,\n"
		"the skeletons would be upon you.\n"
		"Thinking fast, you swing your golden lute back and hurl it at Koops’ head.  At\n"
		"the very least, maybe you can knock her out and make her lose control of her\n"
		"skeletons.\n"
		"However, when the lute is mere inches away from Koops, she reaches up and\n"
		"snatches it out of the air.  Then she charges you, clubbing you in the head so\n"
		"hard that it leaves a face-shaped indent in the side of the instrument.  You\n"
		"would care about the damage done to your precious instrument, but you’re\n"
		"unconscious, spiraling through the air towards the awaiting fire.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch4_Scene4::flee()
	{
		string s = \
		"\n"
		"You turn around and try to run away, scrabbling at the treasure pile, but all\n"
		"the coins and trinkets keep sliding around, making you lose your grip.  It’s\n"
		"obvious there’s no escape this way.\n"
		"“Come now, Thaddeus.  Now is not the time for fear!  That comes later,” Koops\n"
		"intones.\n"
		"Giving up on your attempted escape, you turn back around and face her once more.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch4_Scene4::interact()
	{
		string s = \
		"\n"
		"You look around frantically for something usable in the environment.\n"
		"Maybe the bridge is retractable, like in those games with the plumber and the\n"
		"turtle!  But alas, you can’t find anything useful that will save you.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch4_Scene4::talk()
	{
		string s = \
		"\n"
		"“Can’t we all just talk this out?” you yell.\n"
		"“No, we most certainly cannot!” Koops responds.  “You can’t just walk out!\n"
		"Your punishment must be more severe!  You know my true identity, therefore,\n"
		"you must die.”\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}

	void Ch4_Scene4::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 4;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			protagonist->removeItem("MATCHES", 1);
			string s = \
			"\n"
			"You light a match and throw it at the skeletons.  It sits on the walkway,\n"
			"smoldering for a minute before one of them walks over it, stomping it out.  So\n"
			"much for that idea. \n";
			typeText(s, 25000);
			stageValue = 4;
			completed = false;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			string s = \
			"\n"
			"You pull out the crystal ball and hurl it at the skeletons.  It pegs one in the\n"
			"head and it falls off the side of the walkway, screaming as it goes.  The other\n"
			"skeletons pause for a moment and then look back at you.\n"
			"“OH MY GOSH!  HE KILLED KEN!” one screams.\n"
			"“GET HIM!” screams another as the skeletons charge you, skewering you on\n"
			"hundreds of blades simultaneously.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			protagonist->removeItem("PIXIE-B-GONE", 1);
			string s = \
			"\n"
			"You pull out the can of Pixie-B-Gone and spray it at the skeletons, emptying the\n"
			"entire can.\n"
			"On the bright side, you found out that they aren’t pixies.\n"
			"On the other hand, it should have been pretty obvious.\n";
			typeText(s, 25000);
			stageValue = 4;
			completed = false;
		}
		if(userChoice == "BERRY")
		{
			protagonist->removeItem("BERRY", 1);
			string s =\
			"\n"
			"You reach into your pocket and pull out a berry.  Chewing it quickly, you find\n"
			"that it has a somewhat nutty flavour to it.\n"
			"“Enjoy your last meal, gnome!” Koops calls in a mocking voice.\n";
			typeText(s, 25000);
			stageValue = 4;
			completed = false;
		}
		if (userChoice == "GOLDEN LUTE")
		{
			protagonist->removeItem("GOLDEN LUTE", 1);
			string s = \
			"\n"
			"You stare down at the golden lute in your hands.  This is what you’ve been\n"
			"searching for.  You’ve braved so many dangers for it, and now it’s about to be\n"
			"worth nothing.  You decide that you may as well make it worthwhile and go down\n"
			"playing music, like your father would have wanted.\n"
			"Leaning back against the treasure pile, you begin strumming out some chords.\n"
			"It feels like forever since you last played, but as you pluck away at the\n"
			"strings, it all comes back to you.\n"
			"Suddenly, you stand up and launch into a rendition of ‘Gnomehemian Rhapsody’,\n"
			"frantically picking away at the strings and dancing along to the tune.\n"
			"Then something strange happens.  The skeletons stop their advance, look at each\n"
			"other, and start dancing along to the music.\n"
			"“What?  No!  What are you doing?” cries Koops.  “Kill the intruder and then we\n"
			"can celebrate!”\n"
			"But the skeletons aren’t paying attention, surrounding Koops and picking her up\n"
			"with their bony fingers.  Then they start passing her around as though she were\n"
			"crowd-surfing at a concert.\n"
			"“What in the world is going on?” you exclaim.\n"
			"Koops doesn’t pay any attention to your outburst and keeps struggling to free\n"
			"herself, pushing and kicking the skeletons.  Every time she knocks one over,\n"
			"though, more take its place.  However, as you keep playing, she suddenly strikes\n"
			"out at one and tries to stand up.  As soon as she does, a new skeleton takes the\n"
			"old one’s place, propping her up.  However, this sudden change in footing throws\n"
			"her off balance.  As you watch, she falls to the side and tumbles off the\n"
			"skeletons’ outstretched hands, flying off the walkway and down into the flames\n"
			"below, screaming the whole way down.\n"
			"Shocked, you stop playing.  The skeletons look at you for a second expectantly,\n"
			"but then suddenly disappear.  As soon as Koops perished in the fire, all of her\n"
			"summoned henchman perished too.\n"
			"As you walk out of the fortress, a part of you feels sadness.  You get the\n"
			"feeling that there was some good left inside of Koops, and it may have been\n"
			"possible to draw it out.\n"
			"And then you remember how she was acting.  The way she was so determined to kill\n"
			"you.  All the looting and pillaging.  It’s obvious that she was too far gone to\n"
			"ever come back to reality.\n"
			"With this thought in your mind, you journey back to the village, and tell\n"
			"everyone of your amazing quest.  Nobody believes you at first, claiming it’s\n"
			"just a flamboyant story from an equally flamboyant bard.  However, then you show\n"
			"them your golden lute.  After seeing this proof, they are all desperate to\n"
			"journey to the mountain themselves and reclaim their belongings.\n"
			"You pass on all the knowledge gained from your journey (“I told you about the\n"
			"mountain goat!” exclaims Balesworth excitedly), and then head home to take a\n"
			"well-deserved rest.\n"
			"It’s been a long day and a long journey, but in the end, against all odds, you\n"
			"succeeded.\n"
			"Without any combat experience whatsoever, you defeated a great evil and became\n"
			"the hero of the village.\n"
			"Congratulations!\n"
			"The End.";
			typeText(s, 25000);
			stageValue = 0;
			completed = true;
			protagonist->setGameCompleted();
		}
	}

	void Ch4_Scene4::walk()
	{
		string s = \
		"\n"
		"You decide that there’s nowhere you can go to save yourself and admit defeat.\n"
		"You slowly stroll towards the skeletons and accept your fate.\n"
		"As you breathe your last, you hear Koops comment, “I see, Thaddeus.  So you do\n"
		"not fear death.”\n"
		"Then everything goes black.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}
	