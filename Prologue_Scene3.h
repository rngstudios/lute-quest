//Prologue_Scene3.h

#ifndef PROLOGUE_SCENE3_H
#define PROLOGUE_SCENE3_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Prologue_Scene3 : public PlayerChoiceStage
{
public:


	Prologue_Scene3(Player * p);
	~Prologue_Scene3();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};



#endif