//Ch2_Scene3.cpp
#include "Ch2_Scene3.h"

using namespace std;


	Ch2_Scene3::Ch2_Scene3(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 2: Scene 3\n";
		introduction = \
		"\n"
		"“What’s yer name, strainjah?” the bear asks, with a heavy accent.\n"
		"“Um...It’s Thaddeus.\n”"
		"“Thaddeus, eh?  And brings ya around here then, Thaddeus?”\n"
		"“Well, um, I’m travelling to the Smokey Mountain to Koops’ castle.”\n"
		"The bear lets out a roar, “KOOPS?!?!  Don’t tell me ya work fer that so-called\n"
		"wizard!”  He crouches, almost as if he’s about to pounce on you.  Can bears\n"
		"pounce?  You’re not really sure you want to find out.\n"
		"“Nonono!  No, sorry!  I don’t!  Please don’t hurt me!” you cry.\n"
		"“Aw, okay.  I buhlieve ya, Thaddeus,” says the bear with a grin.  “But in that\n"
		"case, wha on earth do ya want tuh go tuh Koops’ castle?”\n"
		"“Well, his skeleton army stole my golden lute – it’s a musical instrument.”\n"
		"“Oh yeah, ah know what it is!  Ah’ve eaten tons o’ bards what come travelin’\n"
		"through here carryin’ those things!”  He gets a wistful look in his eye as if\n"
		"remembering a better time.\n"
		"“Oh...Um...I really didn’t need to know that,” you mumble.\n"
		"“Well anyway, the reason ah’m not a big fan o’ that Koops fella is that his\n"
		"skeltons keep runnin’ through the forest in the dead o’ night.  Do you have anay\n"
		"ahdea how tuff it is tuh fall asleep when all ya can hear is the sound of bones\n"
		"rattlin’?  Them tasteh, tasteh, bones?”\n"
		"“I...I can’t even imagine.  Tasty bones, yeah...” you stammer out.\n"
		"“Well, ah guess what ah’m askin’ is if you could find a way tuh stop Koops from\n"
		"summonin’ more skeltons while yer up there.  It’d really help me owt.”\n"
		"You stop and think about it for a second.  You’re not much of a fighter, so you\n"
		"don’t think you could defeat Koops.  But who knows, maybe you can talk some\n"
		"sense into him?  Or at least get him to re-route his army around the forest?\n"
		"But what if you can’t?  What if you say you’ll fix things and you don’t and on\n"
		"your way back into the village this bear finds you and eats you?  Your mind\n"
		"racing, you decide to tell the bear:\n";
	}

	Ch2_Scene3::~Ch2_Scene3(){}

	void Ch2_Scene3::getHelp()
	{
		string s = \
		"Enter 1 to select YES.\n"
		"Enter 0 to select NO\n"
		"Enter SAVE to save your game and quit.\n";
		typeText(s, 50000);
	}
	
	void Ch2_Scene3::choice_1()
	{
		string s = \
		"\n"
		"“Sure, I can help you!  In case you haven’t noticed, I’m not exactly a strong,\n"
		"fearsome warrior, but maybe I can talk some sense into Koops!” you tell the\n"
		"bear.\n"
		"“Hey, thas’ great!  Thas’ all ah’m askin’ anyway!” the bear says.  “Ah wish\n"
		"there was sumthin’ ah could do tuh repay you.”\n"
		"“Well, don’t thank me yet!” you say, “I doubt it’ll be easy...”\n"
		"However, the bear doesn’t seem to be paying attention to you, instead rummaging\n"
		"around in the bushes. \n"
		"“Here we go!” he shouts, pulling out a handful of fruit from the tree.  “These\n"
		"here are sum o’ mah fav’rite bearries in the whole wood!  Ya wouldn’t believe\n"
		"how many travlers ah’ve killed tuh protect these!”\n"
		"“Once again, I DID NOT need to know that,” you mutter, taking the berries.\n"
		"\n-----------------------------*You got 3 BERRIES!*------------------------------\n\n"		
		"“Thank you so much, though!  I really appreciate it!”\n"
		"“Ah, it’s only three of ‘em.  Ah’d say it bearly constitutes a gift.”\n"
		"“No, really, it’s great!  Thank you!” you say as you start to walk away.  Then\n"
		"you stop for a second.  “I just realized, I don’t even know your name, Mr. Bear.\n"
		"I’ve just been referring to you as ‘the bear’ in all my internal monologues.”\n"
		"“Oh, mah ahpologies!  The name’s Barry.”\n"
		"“Well, it was great to meet you, Barry!  Thank you for everything!”\n"
		"“Anytime mah friend, anytime!”\n"
		"As you set off once more, you carefully stow the berries from Barry the bear in\n"
		"your pocket, making sure they don’t get crushed.\n";
		protagonist->addItem("Berry", "A round, juicy-looking berry.  It has a light blu-ish tint.", 3);
		typeText(s, 25000);
		stageValue = 4;
		completed = true;

	}

	void Ch2_Scene3::choice_0()
	{
		string s = \
		"\n"
		"“You know what, I really don’t think I can commit to something like that.  This\n"
		"whole journey has me scared enough without trying to take out or talk down an\n"
		"evil wizard in the process.  I’m really hoping to just sneak in, get my lute,\n"
		"and get out,” you tell the bear.\n"
		"“Aw shucks, really?  Well, ah guess ah won’t force ya.”\n"
		"“Sorry about that...” you mumble as you step away.\n"
		"“Naw, it’s fine.  Hopefully sum warrior with crazy hair and a big sword or\n"
		"sumthin’ will come through and get rid of Koops later,” the bear mutters.  “See\n"
		"ya later, then.”\n"
		"“Yeah, see you.”\n"
		"You slowly wander further into (or is it out of?) the forest as the bear\n"
		"shuffles back into the bushes.  You feel a bit sad that you couldn’t help, but\n"
		"relieved that you didn’t have to take on even more responsibility.\n";
		typeText(s, 25000);
		stageValue = 4;
		completed = true;
	}
	