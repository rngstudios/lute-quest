//Ch4_Scene4.h

#ifndef CH4_SCENE4_H
#define CH4_SCENE4_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch4_Scene4 : public PlayerChoiceStage
{
public:


	Ch4_Scene4(Player * p);
	~Ch4_Scene4();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif