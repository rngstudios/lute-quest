//Stage.cpp

#include "Stage.h"

Stage::~Stage() {}

bool Stage::isCompleted(){
	return completed;
}

void Stage::resetStageCompletedValue()
{
	completed = false;
}
	
std::string Stage::getIntro()
{
	return introduction;
}

void Stage::printText(std::string s)
{
	std::cout << s << std::endl;
}

void Stage::textBreak()
{
	std::cout << "________________________________________________________________________________" << std::endl;
	std::cout << std::endl;
}

void Stage::typeText(std::string s, unsigned int speed)
{
	unsigned int i = 0;
	while(i<s.length())
	{
		if(s.at(i)=='*')
		{
			std::cout << s.at(i) << std::flush;
			usleep(300000);			
		}
		else if((s.at(i) =='.') || (s.at(i)=='!') || (s.at(i)=='?'))
		{
			//usleep(300000);
			std::cout << s.at(i) << std::flush;
			usleep(300000);
		}
		else if(s.at(i)==',')
		{
			std::cout << s.at(i) << std::flush;
			usleep(200000);
		}				
		else if((s.at(i)==' ') || (s.at(i)=='|') || (s.at(i)=='_'))
		{
			std::cout << s.at(i) << std::flush;
		}
		else
		{
			usleep(speed);
			std::cout << s.at(i) << std::flush;
		}
		i++;
	}
	usleep(s.length()*100);
	std::cout << std::endl;
}

void Stage::clearScreen()
{
	std::string clearScreen;
	for(int i = 0; i < 60; i++)
	{
		clearScreen = clearScreen + "\n";
	}
	printText(clearScreen);
}

bool Stage::isGameSaved() {
	return gameSaved;
}

void Stage::setGameSaved(bool val) {
	gameSaved = val;
}

void Stage::gameOver()
{
	usleep(6000000);
	std::string s;
	s = \
	" ______________________________________________________________________________ \n"
	"|                                                                              |\n"
	"|  @@@@@@@@@@         @@           @@@      @@@     @@@@@@@@@                  |\n"
	"| @@        @@       @@ @@        @@ @@    @@ @@    @@                         |\n"
	"| @@                @@   @@      @@   @@ @@    @@   @@                         |\n"
	"| @@      @@@@@    @@@@@@@@@    @@    @@ @@     @@  @@@@@@@                    |\n"
	"| @@         @@   @@       @@   @@     @@@      @@  @@                         |\n"
	"|  @@       @@@  @@         @@  @@              @@  @@                         |\n"
	"|   @@@@@@@@@    @@         @@  @@              @@  @@@@@@@@@                  |\n"
	"|                                                                              |\n"
	"|                               @@@@@      @@       @@   @@@@@@@@@  @@@@@@@    |\n"
	"|                             @@     @@    @@       @@   @@         @@    @@   |\n"
	"|                            @@       @@   @@       @@   @@         @@     @@  |\n"
	"|                            @@       @@    @@     @@    @@@@@@@    @@@@@@@@   |\n"
	"|                            @@       @@     @@   @@     @@         @@    @@   |\n"
	"|                             @@     @@       @@ @@      @@         @@     @@  |\n"
	"|                               @@@@@          @@@       @@@@@@@@@  @@      @@ |\n"
	"|                                                                              |\n"
	"|______________________________________________________________________________|\n";

	std::string message = "\n*Returning you to the beginning of the Scene...*\n";
	std::string clearScreen;
	for(int i = 0; i < 60; i++)
	{
		clearScreen = clearScreen + "\n";
	}
	printText(clearScreen);
	typeText(s, 5000);
	usleep(5000000);
	printText(clearScreen);
	typeText(message, 50000);
	usleep(3000000);

}

void Stage::setCompleted(const bool val) {
	completed = val;
}


