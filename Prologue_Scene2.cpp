//Prologue_Scene2.cpp
#include "Prologue_Scene2.h"

using namespace std;

	Prologue_Scene2::Prologue_Scene2(Player* p)
		:protagonist(p)
	{
		title = "Prologue: Scene 2\n";
		introduction = \
		"\n"
		"You begin to creep down the hallway until you get to the top of the stairs.\n"
		"You can see a strange glow coming from the living room below you, like that of a\n"
		"lantern.\n";
	}

	Prologue_Scene2::~Prologue_Scene2(){}
	
	void Prologue_Scene2::attack()
	{
		string s = \
		"\n"
		"No robber’s going to get the best of Thaddeus the Destroyer (as you like to call\n"
		"yourself)!  You turn around and run into the kitchen, searching for a suitable\n"
		"weapon.  Your chef’s knife glints at you in the darkness.  You quickly grab it\n"
		"and head back to the stairwell.\n"
		"Suddenly you realize that running with a sharp blade is dangerous, so you stop\n"
		"and make sure the blade is pointing down and behind you.  Then you slowly move\n"
		"down the stairs.\n"
		"At the bottom step, you raise the knife above your head and yell,\n"
		"“HEEEERE’S THADDEUS!”\n"
		"However, the living room is completely deserted, with only a sliver of moonlight\n"
		"providing light.  As you look over to the door, you have just enough time to see\n"
		"it swing shut.  You charge out into the cold night, but the street is completely\n"
		"abandoned.  Stepping back inside, everything seems to be in its right place.\n"
		"“I must have been dreaming after all.  Maybe I left the door open last night and\n"
		"someone came by and closed it,” you mumble sleepily.\n"
		"You decide to go back upstairs and fall back asleep, replacing the knife in the\n"
		"kitchen.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene2::flee()
	{
		string s = \
		"\n"
		"Weird glowing things?  Must be aliens!  Or pixies!  Or pixie aliens!\n"
		"Whatever the case, it’s definitely too spooky for you to investigate, so you\n"
		"bolt back upstairs, leap into bed, and shut everything else out, eventually\n"
		"returning to a deep sleep.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene2::interact()
	{
		string s = \
		"\n"
		"You reach out and caress the banister, admiring the high-quality wood grain.\n"
		"Now this is craftsmanship at its finest.  The smoothness of the wood between\n"
		"your fingers, sanded to perfection by the finest gnome carpenters feels almost\n"
		"like silk.\n"
		"Before you can stop yourself, you begin rubbing your face on the banister,\n"
		"feeling its perfection on your cheek.  This must be what heaven feels like!\n"
		"The incredibly soft surface is almost like a pillow.  It’s smoother than the\n"
		"finest chocolate, and softer than down.\n"
		"So smooth that it feels smoother than smooth peanut butter, though not the kind\n"
		"that still has little bits of peanut in it.\n"
		"That’s fake smooth peanut butter.\n"
		"Not-so-smooth peanut butter.\n"
		"And it’s definitely better than that.\n"
		"As you struggle to think of better metaphors, though, you remember that there’s\n"
		"an intruder in your house that you should probably investigate.  Giving the\n"
		"banister one last, longing caress, you make your way down the stairs.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Prologue_Scene2::talk()
	{
		string s = \
		"\n"
		"You attempt to make conversation with your stairwell as you tiptoe down it, but\n"
		"it doesn’t really have much to say.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Prologue_Scene2::use()
	{
		typeText(protagonist->openInventory(), 50000);		
		stageValue = 2;
		completed = false;
	}

	void Prologue_Scene2::walk()
	{
		string s = \
		"\n"
		"You cautiously creep down the stairs, keeping an eye on your footing as you go.\n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}
