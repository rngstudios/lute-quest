//Ch1.h

#ifndef CH1_H
#define CH1_H

#include "Environment.h"
#include "Player.h"

/**
An implementation of the Environment interface
 */

class Ch1 : public Environment
{
public:
	Ch1(Player * p);
	~Ch1();

	int getUID();
};


#endif