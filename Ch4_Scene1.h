//Ch4_Scene1.h

#ifndef CH4_SCENE1_H
#define CH4_SCENE1_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch4_Scene1 : public PlayerChoiceStage
{
public:


	Ch4_Scene1(Player * p);
	~Ch4_Scene1();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif