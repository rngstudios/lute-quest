//Ch1_Scene5.cpp
#include "Ch1_Scene5.h"

using namespace std;

	Ch1_Scene5::Ch1_Scene5(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 1: Scene 5\n";
		introduction = \
		"\n"
		"As he stands up, you see that his hair is wild and unkempt.  His outfit is\n"
		"similar to yours, but with loose seams and threads and sweat stains everywhere.\n"
		"If you didn’t know any better, you would say that he was a homeless gnome.  That\n"
		"impression is amplified all the more by the crazed look he has in his eye.\n"
		"You’re not sure if he’s just feeding off the existing frenzy in the village or\n"
		"if there’s some sort of vegetation at work, but you’re certainly not wanting to\n"
		"spend any great amount of time around him.\n"
		"“So I hear you’re going to Koops’ castle!” he repeats, spittle flying with\n"
		"every ‘s’.\n"
		"“Yeah, I need to go and get back...” You trail off, remembering the incident\n"
		"with Phillipe.  “...some of my stuff.”\n"
		"“Oh geez!  That’s pretty awesome!  Well before you go, you should know that\n"
		"there’s been some sightings of a biiiig ol’ mountain goat on the mountains that\n"
		"likes to follow the sun!” he says.\n"
		"“Reeeeally, now?” you say, skeptically.\n"
		"“Yeah!  I seen him when I went to the tavern one night, just leaving the area!”\n"
		"“Okay, well, thanks for letting me know about that, Balesworth,” you reply.\n"
		"“No problemo!”\n"
		"Your suspicions about the involvement of *ahem* “herbal supplements” confirmed,\n"
		"you walk out of the town gate.  As you do, you see the mayor of the town,\n"
		"Jensen, sitting on his front porch, or rather what remained of it.  Since he was\n"
		"the wealthiest gnome in town, he had insisted that his whole house be built out\n"
		"of the rarest metals and gems.  Apparently that plan had backfired, as the\n"
		"skeletons seemed to have uprooted his house and taken all but the handful of\n"
		"gold bars that made up Jensen’s bed.\n"
		"As you pass by, you can hear Jensen softly repeating, “I never asked for this,”\n"
		"as he hugs a bar of gold close to his face.  You feel thankful that you at least\n"
		"have a home to return to.\n";
	}

	Ch1_Scene5::~Ch1_Scene5(){}
	
	