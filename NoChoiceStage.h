//NoChoiceStage.h


#ifndef NOCHOICESTAGE_H
#define NOCHOICESTAGE_H

#include "Stage.h"

/**NoChoiceStage inherits from Stage Class and acts as an adapter to provide a structure for all the
 implementations of Stage where the user has no options, but where the 
 execution of runStage() is the same. Each child of NoChoiceStage must define only the destructor
 */
class NoChoiceStage : public Stage
{
public:
///Pure virtual destructor to ensure each child individually handles deletion of objects created
	virtual ~NoChoiceStage() = 0;

///runStage is a structured execution of Stages of type NoChoiceStage	
	int runStage();

};


#endif