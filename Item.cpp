//Item.cpp

#include "Item.h"

Item::Item(std::string n, std::string d, int q){
	name = n;
	description = d;
	quantity = q;
}

std::string Item::getName()
{
	return name;
}

std::string Item::getDesc()
{
	return description;
}

Item::~Item()
{
	
}