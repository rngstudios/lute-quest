//Ch1.cpp

#include "Ch1.h"
#include "Ch1_Scene1.h"
#include "Ch1_Scene2.h"
#include "Ch1_Scene3.h"
#include "Ch1_Scene4.h"
#include "Ch1_Scene5.h"

Ch1::Ch1(Player * p)
{
	title = \
	"\n                            ~~Chapter 1: A New Day~~                            \n";

	stages.push_back(new Ch1_Scene1(p));
	stages.push_back(new Ch1_Scene2(p));
	stages.push_back(new Ch1_Scene3(p));
	stages.push_back(new Ch1_Scene4(p));
	stages.push_back(new Ch1_Scene5(p));
}

Ch1::~Ch1()
{
	for(unsigned int i = 0; i < stages.size(); i++)
	{
		delete stages[i];
	}
}


int Ch1::getUID() {
	return 1;
}

