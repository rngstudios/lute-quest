//Ch1_Scene1.cpp
#include "Ch1_Scene1.h"

using namespace std;

	Ch1_Scene1::Ch1_Scene1(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 1: Scene 1\n";
		introduction = \
		"\n"
		"You wake up in the morning after a restless sleep.  After lying in bed groggily\n"
		"for a few minutes, the events of the night come back to you.  Immediately, you\n"
		"run downstairs to see if you can piece anything together in daylight.\n"
		"As you stare around your living room, everything seems normal.  The red velvet\n"
		"carpet on the floor is slightly rumpled, but all of the furniture seems to be in\n"
		"order.\n"
		"However, then you look in the corner of the room, and get a sinking feeling in\n"
		"the pit of your stomach.\n"
		"It’s gone!  Your golden lute!  The lute that was passed down to you by your\n"
		"father, Matthew, who received it from his father, Mark, who received it from his\n"
		"father, Luke, who received it from his father, John, who received it from his\n"
		"father...Well, at that point the family tree got a bit weird.  People had way\n"
		"longer names like Cor-something-or-other.\n"
		"Point being, it’s a priceless family heirloom!\n"
		"Plus, it had the best strings on it out of all of your lutes.  Being a bard, of\n"
		"course you have many, but only the golden lute had strings made of elven silk.\n"
		"In fact, the strings were so soft that your fingers never got tired of playing\n"
		"it!  And now it’s gone!  Nobody else was in your house last night to the best of\n"
		"your knowledge, so it must have been that skeleton!\n"
		"As you come to this conclusion, you dash up the stairs to your room and get\n"
		"dressed, throwing on your black pants, soft blue sweater, and bright red hat.\n"
		"You check your pocket to confirm that you have your emergency book of matches\n"
		"and walk out into the hallway.\n"
		"\n-----------------------------*You got 50 matches!*------------------------------\n"
		"\n"
		"Doing up your belt as you go back down the stairs, you step into your shiny\n"
		"black boots and swing the door open...\n"
		"...only to be met with utter chaos.\n"
		"The entire village is out in the streets!  People are shouting at each other,\n"
		"tackling each other, and assaulting each other with gardening tools.  The\n"
		"streets are running red, not with blood, but with pointy gnome hats gently\n"
		"rolling around brawls.\n"
		"As you stand on your front porch, looking on in awe, your friend Phillipe\n"
		"suddenly runs up to you.\n"
		// Phillipe.
		"“Hey, Thaddeus!  Thank goodness you’re alive!” cries Phillipe.";

	}

	Ch1_Scene1::~Ch1_Scene1(){}
	
	void Ch1_Scene1::attack()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}
		string s = \
		"\n"
		"Without even thinking, you punch Phillipe in the face.  You’ve had a crazy\n"
		"enough night without having people jump you in the streets!  Unfortunately, some\n"
		"nearby gnomes don’t seem to think that gives you the right to assault your best\n"
		"friend and leap on top of you.  You try to fight your way out, but their beards\n"
		"are too thick, and you suffocate in a pile of sweaty, white, gnome beard hair.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch1_Scene1::flee()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}
		string s = \
		"\n"
		"You scream and run back into your house.  WHERE THE HELL DID PHILLIPE COME\n"
		"FROM?!?!  THE REAL WORLD IS SCARY!  I’LL JUST NEVER GO OUTSIDE AGAIN!\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch1_Scene1::interact()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}		
		string s = \
		"\n"
		"You give Phillipe a big hug.  “I’m so glad that you’re alive too!” you say,\n"
		"“what a crazy night!” \n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}

	void Ch1_Scene1::talk()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}		
		string s = \
		"\n"
		"“Geez, glad that you’re alive too!” you say, turning to Phillipe.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}

	void Ch1_Scene1::use()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}		
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 1;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			string s = \
			"\n"
			"You whip out your book of matches, strike one, and throw it on Phillipe.\n"
			"You feel an odd sense of satisfaction watching your friend run screaming\n"
			"through the village streets, as he collides with people and spreads the\n"
			"inferno.\n"
			"Perhaps a part of you has just died.  Or perhaps you have been born anew.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
	}

		

	void Ch1_Scene1::walk()
	{
		if (!protagonist->checkInventory("Matches"))
		{
			protagonist->addItem("Matches", "A small book of matches. You carry them with you in case of emergencies.", 50);
		}		
		string s = \
		"\n"
		"Ignoring Phillipe, you start walking down the street.  However, he must have\n"
		"assumed you didn’t hear him, because he follows after you and continues talking.\n"
		"You decide to stop and listen to what he has to say.  He is your friend, after\n"
		"all.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}
