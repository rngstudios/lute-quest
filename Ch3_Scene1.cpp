//Ch3_Scene1.cpp
#include "Ch3_Scene1.h"

using namespace std;


	Ch3_Scene1::Ch3_Scene1(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 3: Scene 1\n";
		introduction = \
		"\n"
		"After walking for what feels like an eternity, you realize that the forest seems\n"
		"a little brighter than it did before.  Excited, you pick up the pace.  The trees\n"
		"are thinning!  You’re almost through!  Finally, you step between the last few\n"
		"trees and out into the sunlight.  You feel its warm, golden glow on your skin as\n"
		"it slowly sinks down over the horizon.  The sky is a gorgeous watercolour of\n"
		"pinks, reds, and oranges.  Before you, the Smokey Mountain stands tall, casting\n"
		"its shadow to the east.  The whole scene looks like something out of a dream – \n"
		"the painterly sky, the lush forest behind you, the ground covered in empty\n"
		"bottles of mead, the mountain...Wait, hold on!  Back up!  Empty mead bottles?\n"
		"Sure enough, looking down, you see bottle upon bottle of the originally-titled\n"
		"‘Mountain Mead’.  Geez, whoever thought up that one probably wasn’t one for\n"
		"creativity.  Anyway, every one of the hundreds of bottles is black, except for\n"
		"their labels, and completely empty.  Interestingly, none of them are broken, and\n"
		"upon further examination you can see that the bottles are made of incredibly\n"
		"thick glass – to the point where almost half the volume of the bottle is just\n"
		"glass.  Reaching down, you pick one up and cram it in your pocket.\n"
		"\n--------------------*You got a Mountain Mead Bottle!*-------------------------\n"
		"\nNever know when a bludgeoning implement could come in handy.  Focusing once more\n"
		"on the mountain, you see that there is a narrow dirt path leading straight up\n"
		"for a short distance before branching off into two different paths – one going\n"
		"up the west side of the mountain and the other going up the east side.  Other\n"
		"than the differences in direction, the paths look effectively identical.  Both\n"
		"look like they’ve been walked on an equal amount, and both snake around the\n"
		"mountain so that you can’t really see where they end up.  Which path will you\n"
		"take? \n";

		
	}

	Ch3_Scene1::~Ch3_Scene1(){}

	void Ch3_Scene1::getHelp()
	{
		string s = \
		"Enter 1 to select West.\n"
		"Enter 0 to select East.\n"
		"Enter SAVE to save your game and quit.\n";
		typeText(s, 50000);
	}
	
	void Ch3_Scene1::choice_1()
	{
		if (!protagonist->checkInventory("Mountain Mead Bottle"))
		{
			protagonist->addItem("Mountain Mead Bottle", "An empty dark-brown bottle of Mountain Mead.", 1);
		}		
		string s = \
		"\n"
		"“Well, I’d better try to stay in the sunlight for as long as possible,” you\n"
		"mutter to yourself as you begin trudging up the west path.  The sun beats down\n"
		"on you as the path slowly snakes around the mountain.  Suddenly, you round a\n"
		"corner and are faced with an enormous mountain goat.  Seriously, this thing’s\n"
		"huge!  Like, bigger than three of those bears earlier!  And between you and me,\n"
		"he does not look happy to see you.  You notice that he’s holding a bottle of the\n"
		"Mountain Mead you saw at the base of the mountain.  With one clean motion, he\n"
		"bites the cork, pulls it out, spits it at your face, and guzzles the entire\n"
		"bottle in one gulp.  Knocked over by the force of the cork, you stare at the\n"
		"goat in awe.  His massive horns curl around on the sides of his head, and his\n"
		"furry white coat is covered in dirty patches.  His eyes are bloodshot, and his\n"
		"beard rivals the most elderly of gnomes.\n"
		"“WHAT THE HAIL DO YE THINK YER DOIN’ HERE, LADDIE?!?!” the goat suddenly bellows\n"
		"in a thick, Scottish accent.\n"
		"“Um I um well you um see I...” you stammer out in shock.\n"
		"“THIS HERE’S MY MOONTAIN, AND AH DON’T TAKE KINDLY TO UNINVITED VISITERS SUCH\n"
		"AS YERSELF!” hollers the goat, as he stands up on all fours.\n"
		"“Well yes sir I understand sir I’m sorry!” you cry out, paralyzed with fear.\n"
		"“ALLOW ME TO SHOW YA THE DOOR!” yells the goat, suddenly launching forward with\n"
		"the force of a bullet and smashing into you.  You’re launched screaming into the\n"
		"stratosphere with such force that you complete a full orbit of the world before\n"
		"smashing into the east side of the mountain.  I think it’s safe to say you\n"
		"didn’t survive that.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;

	}

	void Ch3_Scene1::choice_0()
	{
		if (!protagonist->checkInventory("Mountain Mead Bottle"))
		{
			protagonist->addItem("Mountain Mead Bottle", "An empty dark-brown bottle of Mountain Mead.", 1);
		}			
		string s = \
		"\n"
		"“Well, right is right, so east is...best,” you mutter, thankful that nobody was\n"
		"around to hear your attempt at a clever quip.  You look at the sunset one last\n"
		"time before stepping into the shadow of the mountain.  The path is cold and\n"
		"dark, but at least it seems like there’s nothing to stand in your way.  Maybe\n"
		"the dark side of the mountain isn’t so bad after all?  As the path works its\n"
		"way up, it also gradually curves outward around the mountain, and then heads\n"
		"around to the south side.  When you get to the south side, you see that the\n"
		"western path meets up with your path and the two continue on around the west\n"
		"side of the mountain.\n"
		"“I wonder why someone even bothered making a second path in the first place,”\n"
		"you wonder as you continue climbing.  “Seems like the west path would have\n"
		"gotten me here just fine.”\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}
	