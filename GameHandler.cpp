//GameHandler.cpp

#include <fstream>
#include <iostream>
#include "TextAdventureGame.h"
#include "GameHandler.h"
#include <limits.h>
#include <unistd.h>
#include "Prologue.h"
#include "Ch1.h"
#include "Ch2.h"
#include "Ch3.h"
#include "Ch4.h"

GameHandler::GameHandler(TextAdventureGame *textGame) : game{textGame} {

}

GameHandler::~GameHandler() {
    delete game;
}

void GameHandler::readFromFile(const std::string pathToFile) {
    std::cout << "Read from file : " << pathToFile << std::endl;
    std::ifstream inFile(pathToFile);

    std::string tmpLine;
    std::vector<std::string> fileLines;
    while (getline(inFile, tmpLine)) {
        fileLines.push_back(tmpLine);
    }

    inFile.close();

    std::string type;
    std::string fieldName;
    std::string value;
    std::string desc;
    std::string qty;

    for (std::string strLine : fileLines) {
        /*
         * Get the type
         */
        type = grabString(strLine, "(", ":", 0);

        /*
         * Get the variable name
         */
        fieldName = grabString(strLine, ":", ")=", 0);

        /*
         * Get the value
         */
        if (type != "Item") {
            value = grabString(strLine, "{", "};", 0);
        } else {
            desc = grabString(strLine, "{(desc:", "),", 6);
            qty = grabString(strLine, ",(", ")};", 5);
        }

        /*
         * Creating the objects based on values from file
         */
        if (type == "Player" && fieldName == "name") {
            delete game->getPlayer();
            game->setPlayer(new Player(fieldName));
        } else if (type == "Environment") {
            if (fieldName == "id") {
                switch (std::stoi(value)) {
                    case 0:
                        game->addEnv(new Prologue(game->getPlayer()));
                        game->addEnv(new Ch1(game->getPlayer()));
                        game->addEnv(new Ch2(game->getPlayer()));
                        game->addEnv(new Ch3(game->getPlayer()));
                        game->addEnv(new Ch4(game->getPlayer()));
                    break;

                    case 1:
                        game->addEnv(new Ch1(game->getPlayer()));
                        game->addEnv(new Ch2(game->getPlayer()));
                        game->addEnv(new Ch3(game->getPlayer()));
                        game->addEnv(new Ch4(game->getPlayer()));
                    break;

                    case 2:
                        game->addEnv(new Ch2(game->getPlayer()));
                        game->addEnv(new Ch3(game->getPlayer()));
                        game->addEnv(new Ch4(game->getPlayer()));
                    break;

                    case 3:
                        game->addEnv(new Ch3(game->getPlayer()));
                        game->addEnv(new Ch4(game->getPlayer()));
                    break;

                    case 4:
                        game->addEnv(new Ch4(game->getPlayer()));
                    break;

                    default:
                        std::cout << "Something went wrong with the reconstruction ?! :'(" << std::endl;
                    break;
                }
            } else if (fieldName == "stageIndex") {
                game->setStageIndex(std::stoi(value));
            }
        } else if (type == "Item") {
            game->getPlayer()->addItem(fieldName, desc, std::stoi(qty));
        }
    }
}

std::string GameHandler::grabString(const std::string parent, const std::string start, const std::string end, const int shiftStart) {
    std::string value;
    std::string::size_type startPosition = 0;
    std::string::size_type endPosition = 0;

    startPosition = parent.find(start);
    if (startPosition != std::string::npos) {
        ++startPosition;
        endPosition = parent.find(end);
        if (endPosition != std::string::npos) {
            startPosition += shiftStart;
            value = parent.substr(startPosition, endPosition - startPosition);
        }
    }

    return value;
}

void GameHandler::writeToFile(const std::string fileName) {
    std::ofstream out(fileName);

    /*
     * Writing Player Attributes
     */
    out << "(Player:name)={" << this->game->getPlayer()->name << "};" << "\n";

    /*
     * Writing Environment Attributes
     */
    out << "(Environment:id)={" << this->game->getActiveEnv()->getUID() << "};" << "\n";
    out << "(Environment:stageIndex)={" << this->game->getActiveEnv()->getStartingStageIndex() << "};" << "\n";

    /*
     * Writing Inventory Attributes
     */

    if (this->game->getPlayer()->inventory.size() >= 1)
    {
        for (Item* item : this->game->getPlayer()->inventory)
        {
            out << "(Item:" << item->getName() << ")={" <<
                "(desc:" << item->getDesc() << ")," <<
                "(qty:" << item->quantity << ")};" << "\n";
        }
    }


    out.close();
}

std::string GameHandler::getDir() {
    char result[PATH_MAX];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
    std::string dir = std::string( result, (count > 0) ? count : 0 );
    dir.erase(dir.length() - 13);
    return dir;
}

void GameHandler::gameLoop() {
    while (!this->game->isOver()) {
        std::string command;
        if (!ranOnce)
        {
            ranOnce = true;
            do {
                std::cout << "Do you want to load a LuteQuest save file? (y/n): ";
                std::getline(std::cin, command);
            } while (command != "y" && command != "n");

            if (command == "y")
            {
                game->setLoadedGame(true);
                std::string saveFileName;
                do {

                    std::cout << "Please enter the name of your save file without the .lute extension.\nIf you want to start a new game, please relaunch the game and enter 'n'.\n\n";
                    std::cin >> saveFileName;
                    std::cin.clear();
                    std::cin.ignore(10000, '\n');
                    saveFileName.append(".lute");
                    saveFileName = getDir() + saveFileName;

                } while (!doesFileExist(saveFileName));

                readFromFile(saveFileName);
                command = "n";
            }
        }

        if (command == "n")
        {
            std::cout << "Loading Game..." << std::endl;
            this->game->play();
            int index = this->game->getActiveEnv()->getStartingStageIndex();
            if (index != -1 && this->game->getActiveEnv()->getActiveStage()->isGameSaved()) {
                this->writeToFile("saveFile.lute");
                this->game->getActiveEnv()->getActiveStage()->setGameSaved(false);
                std::cout << "Your game has been saved to the file saveFile.lute!  Exiting." << std::endl;
                break;
            }
            return;

        }
    }
}

bool GameHandler::doesFileExist(const std::string fileName) {
    std::ifstream in(fileName);
    return in.good();
}
