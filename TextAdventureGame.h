//TestAdventureGame.h

#ifndef TEXTADVENTUREGAME_H
#define TEXTADVENTUREGAME_H

#include "Environment.h"
#include "Player.h"
#include <time.h>



class TextAdventureGame {
private:
///Title of the game
    std::string title; 

///Brief introduction unique to a game   
    std::string introduction;

///Pointer to an instance of the (User) Player
    Player* Thaddeus;

///Pointer to the environment(chapter) which the (User) Player is currently in
    Environment* activeEnvironment;

///A vector of all environments(chapters) in the game
    std::vector<Environment*> envs;

///stageIndex represents the scene within the environment(chapter) that the (User) player is in
    int stageIndex = 0;

///loadedGame is used to notify the Game Handler whether or not the current game is a new version or a loaded version
    bool loadedGame = false;

public:	
	TextAdventureGame();

	~TextAdventureGame();

///This method executes the activeEnvrinoment and iterates through the Stages within that chapter
	void play();

///Returns whether or not the current environment has been completed
	bool environmentComplete();

///Print function that prints without any time delay, used for clearing screen
	void printText(std::string s);
	
/**
Print function that prints with a time delay (in microseconds) for each character that is not a type of 
punctuation, no time delay for spaces (' '), and a standard delay for punctuation
*/
	void typeText(std::string s, unsigned int speed);

///Prints 60 newlines to clear terminal screen
	void clearScreen();

///Pauses output until user confirms to continue
	void userContinue();

///gameBeaten() is used to manage cases where the user has beaten a game, and wishes to play again immediately without exiting the program
	void gameBeaten();

///Getter method for TextAdventureGame's private introduction member
	std::string getIntro();

///Getter method for TextAdventureGame's private title member
	std::string getTitle();

///Returns the time difference between the two clockType parameter values passed into it
	std::string getTime(clock_t start, clock_t end);

///Takes a string, and returns the same string in all upper cases. Used to standardize comparisons of strings
	std::string convertToUpper(std::string s);

///Returns true when the game is over;
	bool isOver();

///Manually sets the private stageIndex value for when loading an environment which had been partially comopleted
	void setStageIndex(const int val);

///Returns a pointer to Thaddeus, the (User) Player 
	Player* getPlayer();

///Sets 'player' to point to Thaddeus, used for copying during save procedures
	void setPlayer(Player* player);

///Returns a pointer to the current environment which the Game is executing
	Environment* getActiveEnv();

///Sets 'env' to point to the TextAventureGame's current activeEnvironment. Used in saving procedure in Game Handler
	void setEnvironment(Environment* env);

///Returns the list of environments of the TextAdventureGame
	std::vector<Environment*> getEnvs();

///Adds an evironment to the list of environments. Used in saving and loading a partially completed game
	void addEnv(Environment* env);

///Setter method for private member loadedGame
	void setLoadedGame(const bool val);
	
///Returns value indicating if the TextAdventureGame is a previously saved and loaded version or a new version
	bool isLoadedGame();
};


#endif