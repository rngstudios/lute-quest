//Ch3.cpp

#include "Ch3.h"
#include "Ch3_Scene1.h"
#include "Ch3_Scene2.h"
#include "Ch3_Scene3.h"
#include "Ch3_Scene4.h"


Ch3::Ch3(Player * p)
{
	title = \
	"\n                        ~~Chapter 3: The Smokey Mountain~~                         \n";
	stages.push_back(new Ch3_Scene1(p));
	stages.push_back(new Ch3_Scene2(p));
	stages.push_back(new Ch3_Scene3(p));
	stages.push_back(new Ch3_Scene4(p));

}

Ch3::~Ch3()
{
	for(unsigned int i = 0; i < stages.size(); i++)
	{
		delete stages[i];
	}
}

int Ch3::getUID() {
	return 3;
}
