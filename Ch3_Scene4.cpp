//Ch3_Scene4.cpp
#include "Ch3_Scene4.h"

using namespace std;

	Ch3_Scene4::Ch3_Scene4(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 3: Scene 4\n";
		introduction = \
		"\n"
		"“YE THINK THROWIN’ SOME WEE BATS AT ME’LL STOP ME?!?!”\n"
		"The mountain goat continues to drunkenly charge towards you as he lowers his\n"
		"head, plowing through the swarm of bats flying towards him.\n"
		"You turn and make a break towards the peak of the mountain, praying that maybe,\n"
		"just maybe, you’ll be able to outrun the goat.  Looking over your shoulder, you\n"
		"see him come barreling towards the corner.\n"
		"However, as he turns, he loses his footing and slides into the bat cave.\n"
		"Smashing clear through it, he goes flying off the edge of the cliff, plummeting\n"
		"to the ground below.\n"
		"When he hits the ground, he doesn’t stop, though.  He just keeps running,\n"
		"charging through trees, shacks and more that stand in his way.\n"
		"“YE AIN’T SEEN THE LAST OF ME, GNOME SCUM!!!” shouts the goat as he heads toward\n"
		"the horizon, leaving a trail of destruction in his wake.\n"
		"“Well, that was certainly...interesting,” you murmur, “but at least I don’t have\n"
		"to worry about him anymore.”\n";
	}

	Ch3_Scene4::~Ch3_Scene4(){}
	
	