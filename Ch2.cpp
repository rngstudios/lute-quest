//Ch2.cpp

#include "Ch2.h"
#include "Ch2_Scene1.h"
#include "Ch2_Scene2.h"
#include "Ch2_Scene3.h"
#include "Ch2_Scene4.h"


Ch2::Ch2(Player * p)
{
	title = \
	"\n                        ~~Chapter 2: The Bear Necessities~~                         \n";
	stages.push_back(new Ch2_Scene1(p));
	stages.push_back(new Ch2_Scene2(p));
	stages.push_back(new Ch2_Scene3(p));
	stages.push_back(new Ch2_Scene4(p));
}

Ch2::~Ch2()
{
	for(unsigned int i = 0; i < stages.size(); i++)
	{
		delete stages[i];
	}
}



int Ch2::getUID() {
	return 2;
}

