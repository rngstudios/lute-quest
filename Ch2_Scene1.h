//Ch2_Scene1.h

#ifndef CH2_SCENE1_H
#define CH2_SCENE1_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch2_Scene1 : public PlayerChoiceStage
{
public:


	Ch2_Scene1(Player * p);
	~Ch2_Scene1();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();
	int getAttackNumber();

private:
	Player * protagonist;
	int attackNumber = 0;
};
#endif