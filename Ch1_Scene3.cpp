//Ch1_Scene3.cpp
#include "Ch1_Scene3.h"

using namespace std;

	Ch1_Scene3::Ch1_Scene3(Player* p)
		:protagonist(p)
	{
		stageValue = 4;
		title = "Chapter 1: Scene 3\n";
		introduction = \
		"\n"
		"“So anyway, I guess if you’re really adamant that you have to go to Koops’\n"
		"fortress, there’s nothing I can do to stop you.  Once you put your mind to\n"
		"something, I know you never change it,” Phillipe says, looking at you.  “I guess\n"
		"all I can do is try and give you a bit of extra knowledge that might increase\n"
		"your chances of survival.  Basically, while it’s just a rumour, I’ve heard that\n"
		"Koops originally created his skeleton army because he never had any friends.  He\n"
		"wanted to have a posse that he could party with and such.  So he created his\n"
		"skeleton army to keep him company.  I mean, then he did the whole ‘send an evil\n"
		"army of skeletons down into villages to loot and steal’ thing, but I guess he\n"
		"had good intentions in the beginning.  Anyway, I don’t know if that will help\n"
		"you in the long run or not, but it’s worth mentioning.”\n"
		"“Okay, thanks.  I guess that’s good to know!”\n"
		"“Yeah, no problem.  Stay safe out there, okay?”  Phillipe says, with a worried\n"
		"look in his eye. You know I always do.  Thanks, Phillipe,” you say as you give\n"
		"him a manly gnome hug and walk away.\n";
	}

	Ch1_Scene3::~Ch1_Scene3(){}
	
	