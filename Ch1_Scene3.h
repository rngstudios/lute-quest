//Ch1_Scene3.h

#ifndef CH1_SCENE3_H
#define CH1_SCENE3_H

#include "Player.h"
#include "NoChoiceStage.h"

class Ch1_Scene3 : public NoChoiceStage
{
public:


	Ch1_Scene3(Player * p);
	~Ch1_Scene3();
	bool chapterDone;
private:
	Player * protagonist;
};
#endif
