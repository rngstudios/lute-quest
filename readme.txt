 _____________________________
|                             |
|            R N G            | 
|                             |
|        S t u d i o s        |
|            _____            |
|                             |
|        ~~presents~~         | 
|                             |
|   _______________________   |
|                             |
|     L U T E                 |
|         Q U E S T  1.0      |
|                             |   
|   _______________________   |   
|                             |
|_____________________________|


If you are cloning this repository, you will have to compile the game files yourself.
	1. Navigate to the lute-quest directory in a terminal after cloning the repository.
	2. Type 'make' (without quotations) and press Enter/Return.
	3. Type 'LuteQuest2720' (without quotations) and press Enter/Return.
	
For detailed game instructions, please read Manual.pdf.
If you need help in-game, just type 'HELP' (without quotations) and press Enter/Return.

We hope you enjoy Lute Quest!

Currently, Lute Quest is only available for Linux.
A Windows version is hopefully on the way!  Check back for updates!


Credits

	Design Lead - Jordan Florchinger

	Q/A Lead - Lucas Jakober

	Documentation Lead - Trevor Henders

	Lead Code Monkey - Lucas Jakober

	Serialization Wizard - Jordan Florchinger

	Script/Manual Author - Trevor Henders

	Scenario - Trevor Henders

	ASCII Artist - Lucas Jakober

	Music - Ludwig van Beethoven

	Motivational Speakers - Monster, Keystone, Powerade

	Banister Polisher - Trevor Henders

	Gnome Beard Groomer - Jordan Florchinger

	Animal Wrangler - Lucas Jakober

	Skeleton Tamer - Trevor Henders

	
	Cast (in order of appearance)

		Narrator - Trevor Henders

		Thaddeus - Leonardo DiCaprio

		Skeletons - Mr. Bones

		Phillipe - Sean Astin

		Gnome Townsfolk - Peter Dinklage

		Balesworth - Dwayne 'The Rock' Johnson

		Barry - Himself

		The Belligerent, Drunken Mountain Goat - Lucas Jakober

		The Golden Lute - Jordan Florchinger

		Koops - Sierra Grace


	Special Thanks:

		Dr. John Anvik,
		Mark Thom,
		CPSC 2720,
		Avril Lavigne,
		Peter Jackson,
		Eidos Montreal,
		Andrew Gold,
		Bane?,
		Sublime Text 2,

		...and you!  Thank you for playing! 