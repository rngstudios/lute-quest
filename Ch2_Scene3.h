//Ch2_Scene3.h

#ifndef CH2_SCENE3_H
#define CH2_SCENE3_H

#include "Player.h"
#include "BinaryChoiceStage.h"

class Ch2_Scene3 : public BinaryChoiceStage
{
public:
	Ch2_Scene3(Player * p);
	~Ch2_Scene3();
	bool chapterDone;

	void choice_1();
	void choice_0();
	void getHelp();


private:
	Player * protagonist;
};
#endif