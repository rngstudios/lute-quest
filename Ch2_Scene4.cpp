//Ch2_Scene4.cpp
#include "Ch2_Scene4.h"

using namespace std;


	Ch2_Scene4::Ch2_Scene4(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 2: Scene 4\n";
		introduction = \
		"\n"
		"As you continue stepping through the forest, you notice that you feel exhausted.\n"
		"It then occurs to you that you’ve been travelling all day and you haven’t even\n"
		"eaten breakfast!  As soon as this realization hits you, your stomach starts\n"
		"growling ferociously.  You start looking around for something, anything, that\n"
		"you can scrounge up for food.  Luckily, gnomes are expert botanists, and you are\n"
		"quickly able to find some plants and herbs that will keep you going for the rest\n"
		"of the day.  You find a tree to lean up against and sit down with your green\n"
		"sandwich.  A couple of pendragon leaves for bread, a few leegreens and\n"
		"pickersnuff petals for the main contents, some saltsap nectar for garnish, and a\n"
		"handful of caffodils for dessert.  Wiping your mouth on your sleeve, you let out\n"
		"a sigh of contentment and then slowly get up.  As you start to move on, you see\n"
		"that on the back of the tree that you were leaning against, there’s a small\n"
		"hole.  Peering inside, you can see a bright orange mushroom with yellow spots.\n"
		"A citrushroom!  But those are some of the rarest mushrooms around!  You lick\n"
		"your lips as you think of how it might taste.  You’ve always wanted to try one,\n"
		"ever since you were a child!  And here it is, right in front of you! You start\n"
		"salivating as you reach towards the tree.  You can almost taste the tangy citrus\n"
		"flavour!  The delightfully sour aftertaste!  You can’t wait to try it!\n"
		"Suddenly, just as your hand is about to wrap around the mushroom, the hole\n"
		"closes.  Not completely, but firmly enough that your hand is stuck inside the\n"
		"tree.  And then it starts to clamp down more!  The tree is eating your hand!\n";
	}

	Ch2_Scene4::~Ch2_Scene4(){}
	
	void Ch2_Scene4::attack()
	{
		string s = \
		"\n"
		"You frantically start beating the tree with all your might.  You punch it with\n"
		"your free hand and kick it, but each limb somehow manages to find its way into\n"
		"a different hole in the tree, getting trapped as well!  With all four limbs\n"
		"stuck in the tree, you have no choice but to cry out in pain and accept your\n"
		"fate as the tree slowly consumes your hands and feet.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene4::flee()
	{
		string s = \
		"\n"
		"You pull as hard as you can, trying to free your hand and run.  The tree\n"
		"continues to increase the pressure on your arm.  You start screaming in agony\n"
		"as you give one last, final pull.  And then you’re free!  You did it!  You fall\n"
		"onto the ground in front of the tree as the hole closes up completely.  A sharp\n"
		"pain shoots through your leg as you realize that you twisted your ankle on the\n"
		"way down.  Suddenly, you hear a loud cracking sound.  The tree seems like it’s\n"
		"leaning towards you more than it was before.  Maybe it’s just your imagination,\n"
		"though.  Then, in one swift motion, the tree tumbles down and smashes your face\n"
		"into the dirt.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene4::interact()
	{
		string s = \
		"\n"
		"You frantically look around the tree for some sort of switch or button.\n"
		"“There’s got to be an off-switch!  There’s always an off-switch!” you cry.\n"
		"There is no off-switch.  \n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene4::talk()
	{
		string s = \
		"\n"
		"“Can you please let me go, Mr. Tree?” you ask, “I really don’t want to lose my\n"
		"hand!”\n"
		"“OMNOMNOMNOM TASTY HANDS” the tree intones.\n"
		"Shoot, who knew trees could talk too?  First bears and now this?  What is the\n"
		"world coming to?  By now, you’re so distracted that you don’t even realize\n"
		"you’ve lost your hand, fallen on the ground, bled out, and died.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch2_Scene4::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 4;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			protagonist->removeItem("MATCHES", 1);
			string s = \
			"\n"
			"You reach into your pocket and pull out a match.  Striking it on the bark of the\n"
			"tree, you hold the flame close to the bark.  You could swear that you hear a\n"
			"horrid screeching sound before the tree quickly releases you.  You fall to the\n"
			"ground and look up in time to see the citrushroom dissolve into a horde of\n"
			"pixies.  They must have been manipulating the tree!  That’s why it suddenly came\n"
			"to life and attempted to eat your hand!  Your thoughts are suddenly interrupted\n"
			"by the smell of smoke.  You look down and are nearly blinded by the blaze that\n"
			"is your beard.  With a screech, you roll over, plant your face in the muddy \n"
			"ground, and start rolling back and forth.  You can hear the cackles of pixies\n"
			"overhead, but it doesn’t matter to you. Finally, after what seems like an \n"
			"eternity, you sigh and lift your head up. Your face is caked in mud, and all\n"
			"that remains of your once luxurious beard is a charcoal goatee.  Weeping at this\n"
			"horrible misfortune, you press on into the woods. \n";
			typeText(s, 25000);
			stageValue = 0;
			completed = true;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			string s = \
			"\n"
			"You grab the crystal ball and start bashing the tree with it.  However, all that\n"
			"happens is the ball shatters and cuts your one good hand.  Consumed by pain and\n"
			"frustration, you decide to just stop fighting and accept your fate.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			protagonist->removeItem("PIXIE-B-GONE", 1);
			string s = \
			"\n"
			"You fumble around in your pockets until your hand closes on the can of\n"
			"Pixie-B-Gone.  You pull it out, give it a good shake, and spray it at the tree.\n"
			"A shriek of horror comes out of the hole, and the tree suddenly releases its\n"
			"grip on you.  As you lay on the ground, you see the citrushroom shimmer and \n"
			"disappear, replaced by a swarm of coughing, wheezing pixies. You realize that\n"
			"they were animating the tree the entire time!  The mischievous little devils!\n"
			"They fly away, still crying out in disgust. Relieved, you pick up the can and\n"
			"place it in the hole in the tree. Hopefully that’ll repel them for a while and\n"
			"protect any future travellers. You gather up your belongings and carry on,\n"
			"hoping that you’ll be out of the woods before long.  \n";
			typeText(s, 25000);
			stageValue = 0;
			completed = true;
		}
		if(userChoice == "BERRY")
		{
			string s =\
			"\n"
			"You quickly reach into your pocket and pull out a berry.  Popping it into your\n"
			"mouth, you feel a sharp stinging sensation in your arm.\n"
			"“Whoa, maybe the berry will help my arm to get free!” you think.\n"
			"Then you realize that the sharp stinging was the feeling of the tree devouring\n"
			"your arm.  At least you had a nice last meal.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
	}

	void Ch2_Scene4::walk()
	{
		string s = \
		"\n"
		"You try to walk away and then remember that your arm is stuck in a tree.  It’s kind\n"
		"of difficult to just walk away from something like that. \n";
		typeText(s, 25000);
		stageValue = 4;
		completed = false;
	}
	