//Ch2_Scene1.cpp
#include "Ch2_Scene1.h"

using namespace std;

	int Ch2_Scene1::getAttackNumber()
	{
		string s = \
		"\n"
		"You have two possible attacks to perform.  Please enter the number ( 1 or 2 )\n"
		"of the attack that you wish to perform.\n"
		"ATTACK 1: Fast Punch\n"
		"ATTACK 2: Strategic Punch\n";
		typeText(s, 50000);
		cin >> attackNumber;
		return attackNumber;
	}

	Ch2_Scene1::Ch2_Scene1(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 2: Scene 1\n";
		introduction = \
		"\n"
		"As you approach the entrance to the forest of Cleerwood, you are in awe of the\n"
		"size of the trees.  From town they looked so small, and yet up close, they tower\n"
		"above the horizon, almost completely blocking the sunlight as you step between\n"
		"them.  Strange rustling noises seem to be all around you, and you swear you see\n"
		"movement out of the corner of your eye.  Everywhere you turn, though, all is\n"
		"still.\n"
		"Trembling, you carefully pick your way through the forest, taking care to step\n"
		"over the massive tree roots that snake over the ground like pythons.\n"
		"As you press on, you are suddenly met with a clearing.  There is a small, grassy\n"
		"circle with what appear to be daisies growing in a few places.  In the centre is\n"
		"a tall oak tree with a hollowed-out trunk.  As you approach the tree, you see\n"
		"that the sunlight filtering through the canopy seems to be reflecting off of\n"
		"something in the trunk.\n";
	}

	Ch2_Scene1::~Ch2_Scene1(){}
	
	void Ch2_Scene1::attack()
	{
		string s;

		attackNumber = getAttackNumber();
		
		switch(attackNumber)
		{
			case 1:
			{
				s += \
				"\n"
				"This tree is obviously an enemy in disguise!  What a dastardly trick!  Without a\n"
				"moment’s hesitation, you punch the tree as hard as you can.  Your fist meets the\n"
				"thick bark, and you swear you hear a crack, but not from the tree.  You scream\n"
				"in agony as you feel the fracture in your finger and wonder why in the world\n"
				"your first instinct upon seeing a tree was to punch it.\n";
				
				stageValue = 1;
				break;
			}
			case 2:
			{
				s+= \
				"\n"
				"You realize that this must be a heavily armoured opponent.  But so early into\n"
				"the game?  What were the devs thinking?  Don’t they know the enemies are\n"
				"supposed to scale in difficulty over time?  This isn’t a difficulty curve, it’s\n"
				"a difficulty cliff!  You decide that there’s nothing you can do about it, grab\n"
				"some leaves, wrap them around your fists, and prepare for your assault.\n"
				"You take a mighty swing at the tree trunk and let out a howl of pain as you feel\n"
				"your knuckles break.  You lie on the forest floor sobbing, wondering why on\n"
				"earth you would even think this was a reasonable course of action.\n"
				"You decide that your golden lute isn’t all that important and instead head\n"
				"back to town.  You have more important personal issues to work out.\n";
				
				stageValue = -1;
				break;
			}
		}

		typeText(s, 25000);
		completed = false;
	}

	void Ch2_Scene1::flee()
	{
		string s = \
		"\n"
		"The tree is obviously a devious trap!  Someone probably hopes that you’ll put\n"
		"your hand inside, only to have it chopped off by a bear trap or something!  But\n"
		"what if they’re watching right now?  What if they attack you anyway for not\n"
		"setting off the trap?\n"
		"These horrific thoughts fill your head and you scream and run away from the\n"
		"tree.  In your panic, though, you must have gotten turned around somewhere,\n"
		"because you end up exactly where you started!  You decide that maybe you\n"
		"should reconsider your approach to the situation.  After all, you still have to\n"
		"get to the mountain, and a hollow tree can’t stop you!  Can it?\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}

	void Ch2_Scene1::interact()
	{
		string s = \
		"\n"
		"You slowly reach into the tree and feel around.  Suddenly, your hand closes\n"
		"around a pair of smooth, round items.  Pulling your hand out, you see that there\n"
		"is a small crystal ball and a can of Pixie-B-Gone!  Someone must have left these\n"
		"here for safekeeping.\n"
		"“Oh well, finders keepers,” you mutter to yourself as you stash the items in\n"
		"your pockets.\n"
		"\n-----------------------------*Got a Crystal Ball!*------------------------------\n"
		"\n--------------------------*Got a can of Pixie-B-Gone!*--------------------------\n"
		"\nMoving on, you leave the clearing and head deeper into the\n"
		"dark forest.\n";
		typeText(s, 25000);
		protagonist->addItem("Crystal Ball", "This ball shines and shimmers in the light.  Watch out!  It's fragile!", 1);
		protagonist->addItem("Pixie-B-Gone", "Apparently it's 'The finest pixie repellent money can buy!'", 1);
		stageValue = 2;
		completed = true;
	}

	void Ch2_Scene1::talk()
	{
		string s = \
		"\n"
		"“Wood you like to tell me what’s in that hole?  I don’t want to bark up the\n"
		"wrong tree!” you tell the tree.\n"
		"The tree stands silently, but you could have sworn that its branches drooped a\n"
		"bit, as though a great sadness suddenly washed over it.\n";
		typeText(s, 25000);
		stageValue = 1;
		completed = false;
	}

	void Ch2_Scene1::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 1;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			string s = \
			"\n"
			"You decide that if anything’s hiding in the tree, you’ll smoke it out.\n"
			"Lighting a match and throwing it into the hole, you stand and wait.  When\n"
			"nothing comes out of the tree, you decide to take a look in.\n"
			"Suddenly, there’s a flash of light and your face is blown clean off.\n"
			"Needless to say, you don’t live long enough to find out what the flash was.\n";
			typeText(s, 25000);
			stageValue = -1;
			completed = true;
		}
	}

	void Ch2_Scene1::walk()
	{
		string s = \
		"\n"
		"“Whatever may be in the tree can’t be that important,” you think to yourself.\n"
		"Instead, you continue on deeper into the forest, leaving the grassy clearing\n"
		"behind you.\n";
		typeText(s, 25000);
		stageValue = 2;
		completed = true;
	}
