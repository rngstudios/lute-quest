//Ch3_Scene4.h

#ifndef CH3_SCENE4_H
#define CH3_SCENE4_H

#include "Player.h"
#include "NoChoiceStage.h"

class Ch3_Scene4 : public NoChoiceStage
{
public:


	Ch3_Scene4(Player * p);
	~Ch3_Scene4();
	bool chapterDone;
private:
	Player * protagonist;
};
#endif
