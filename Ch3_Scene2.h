//Ch3_Scene2.h

#ifndef CH3_SCENE2_H
#define CH3_SCENE2_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch3_Scene2 : public PlayerChoiceStage
{
public:


	Ch3_Scene2(Player * p);
	~Ch3_Scene2();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif