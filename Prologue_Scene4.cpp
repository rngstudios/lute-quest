//Prologue_Scene4.cpp


#include "Prologue_Scene4.h"

using namespace std;

	Prologue_Scene4::Prologue_Scene4(Player* p)
		:protagonist(p)
	{
		title = "Prologue: Scene 4\n";
		introduction = \
		"\n"
		"The glow slowly fades away as the skeleton turns away from you.  You slowly look\n"
		"over the top of the couch to see what the skeleton is looking at, but his eyes\n"
		"suddenly go black, and the room is plunged into darkness.  As your eyes attempt\n"
		"to adjust to the new lighting, you can just barely make out the skeleton\n"
		"grabbing something before turning and running out the door.\n";
	}

	Prologue_Scene4::~Prologue_Scene4(){}
	
	void Prologue_Scene4::attack()
	{
		string s = \
		"\n"
		"With a loud battle cry, you charge towards the door.  However, your eyes have\n"
		"not had time to adjust to the room’s lighting yet, and you trip over a table,\n"
		"slamming your face into the door.  Grunting and groaning, you get up and look\n"
		"outside.  The street is completely abandoned, and there is no sign of the\n"
		"skeleton.  You decide to go back to sleep before you get caught up in even\n"
		"greater danger.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene4::flee()
	{
		string s = \
		"\n"
		"You turn around and bolt up the stairs, praying that the skeleton won’t turn\n"
		"around and see you.  Thankfully, it seems that he was preoccupied with escaping\n"
		"with his prize, whatever that may be.  You crawl back into bed and fall into a\n"
		"dreamless sleep.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene4::interact()
	{
		string s = \
		"\n"
		"You continue hugging the couch, occasionally peering over it to make extra sure\n"
		"that the skeleton left.  Eventually you are overwhelmed with tiredness and make\n"
		"your way back upstairs, thankful that at least you survived this particular bit\n"
		"of excitement.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene4::talk()
	{
		string s = \
		"\n"
		"“There, there, couch.  It’s okay.  I won’t let the spooky skeleton hurt you.”\n"
		"Once you are sure that the couch’s heart rate has gone back to normal levels,\n"
		"you make your way back to bed, hoping that both of you can have a good sleep\n"
		"still.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Prologue_Scene4::use()
	{
		typeText(protagonist->openInventory(), 50000);
		stageValue = 4;
		completed = false;
	}

	void Prologue_Scene4::walk()
	{
		string s = \
		"\n"
		"You walk towards the door and peer outside, however the streets are empty.\n"
		"Deciding that it probably would not be a good idea to go skeleton hunting and\n"
		"stupid-o’clock in the morning, you instead go back into the house, taking extra\n"
		"care to lock the door, and head back to sleep.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}