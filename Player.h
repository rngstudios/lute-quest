//Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Item.h"

/**Class Player inherits from Character and adds functionality by including an list of 
 Items in a vector called 'inventory'. Player class also defines several methods which are 
 unique to a (user) Player and thus not provided in the Character Class.
 */
class Player : public Character
{
public:
///Vector of Item* which each point to a specific item which the player has aquired in the game
	std::vector<Item*> inventory;

/**Represents whether the game has been successfully completed, used in Player class because Player
is common to all aspects of the TextAdventureGame
*/
	bool gameComplete;

public:
/**Constructor sets Player (Character) name = n, and intitializes the remaining fields of Player to 
default values
*/
	Player(std::string n);

///Destructor interates through the list of Items and deletes each item acquired during gameplay
	~Player();

/**Prompts the user for a command and calls convertToUpper to add safety to string comparisons
and widen the range of valid inputs from the user
*/
	std::string getUserCommand();

/**This funtion displays a list of valid options the user may enter, with a brief description of 
each option
*/
	std::string getHelp();

/**Prints out the list of Items the Player has in their inventory, including the item name,
item description, and quantity of that item
*/
	std::string openInventory();

/**Prompts the user to select what item they wish to use after their inventory has been opened. Also
checks that the item does in fact exist in the Player's inventory
*/
	std::string getUserItem();

/**Takes a string as an input and converts it to uppercase. Used to increase safety when comparing strings
the user enters and the string type names of various items and commands
*/
	std::string convertToUpper(std::string s);

///This function creates a new item and adds it to the Player's inventory
	void addItem(std::string name, std::string description, int quantity);

/**Takes and string, converts it to upper case, and them itterates throught the inventory to search for 
the particular item.  Returns true if the search was successful.
*/
	bool checkInventory(std::string itemName);

/**Removes 'n' quantity of an item from the Player's inventory. If the Item quantity reaches 0, the item 
is removed from the inventory all together, and memory managment is taken care of in destructor of Player
*/
	void removeItem(std::string targetItem, int n);

/**Temporarily stops the game and prompts the user to 'Press Enter' in order to continue. Used to improve
playability of game when there are large amounts of text being outputted	
*/
	void userContinue();

///Setter function which sets the private member 'gameComplete'
	void setGameCompleted();

///Getter function which returns the private member 'gameComplete'
	bool gameCompleted();
};



#endif