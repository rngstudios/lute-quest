//Ch3_Scene2.cpp
#include "Ch3_Scene2.h"

using namespace std;


	Ch3_Scene2::Ch3_Scene2(Player* p)
		:protagonist(p)
	{
		stageValue = 0;
		title = "Chapter 3: Scene 2\n";
		introduction = \
		"\n"
		"The path gradually plateaus and you stop for a minute.  The sun is low on the\n"
		"horizon, casting a brilliant reddish glow on the surrounding landscape.  It\n"
		"really is beautiful.  However, it also reminds you that you are fast running out\n"
		"of daylight, so you get back to hiking once again.  As the path winds its way\n"
		"past a ridge, you peer over and look down.  Below you, you see a giant mountain\n"
		"goat, bigger than you ever thought possible.  As you watch, he picks up a nearby\n"
		"bottle of Mountain Mead, guzzles it back, and throws the bottle down to the base\n"
		"of the mountain.  You listen to the clatter of the bottle as it bounces off the\n"
		"rocks and then realize that the goat is glowering up at you.  You smile and wave\n"
		"to him, and in return he holds up a hoof and through some miracle of biology\n"
		"makes an obscene gesture with it.\n";
	}

	Ch3_Scene2::~Ch3_Scene2(){}
	
	void Ch3_Scene2::attack()
	{
		string s = \
		"\n"
		"“WHO THE HELL DO YOU THINK I AM?!?!” you scream as you leap off the ridge onto\n"
		"the goat.  It seems relatively unfazed by your (admittedly, rather impressive)\n"
		"show of force, and instead rolls over in defeat.  Which wouldn’t be a problem,\n"
		"except for the fact that you were on top of it.  As such, when it rolls over, it\n"
		"actually rolls on top of you, crushing you in a sea of booze-scented goat hair.\n";
		typeText(s, 25000);
		stageValue = -1;
		completed = true;
	}

	void Ch3_Scene2::flee()
	{
		string s = \
		"\n"
		"Geez, that goat looks pissed!  It’s like he’s staring into your soul!  You panic\n"
		"and continue running up the mountain, hearing the drunken laughter of the goat\n"
		"behind you.  However, as you run up the mountain, you stumble on some rocks,\n"
		"sending them tumbling behind you.  They go flying over the ridge to the goat\n"
		"below.  Suddenly you hear a mighty roar.\n"
		"“YOU DAAAARE ASSAULT ME, GNOME?!?!” \n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Ch3_Scene2::interact()
	{
		string s = \
		"\n"
		"You decide to match the goat obscenity-for-obscenity.  With all the sass that\n"
		"you can muster, you expertly execute a complicated sequence of hand gestures\n"
		"that would make a drunk dwarvish sailor blush.  The goat appears momentarily\n"
		"impressed, before his inebriated brain catches up and realizes what you just\n"
		"told him.\n"
		"“OH THAT’S IT, GNOME SCUM!!!  YE’RE NOT MAKING IT OFF THIS MOONTAIN ALIVE!”\n"
		"hollers the goat. \n";
		typeText(s, 25000);
		stageValue = 3;
		completed = true;
	}

	void Ch3_Scene2::talk()
	{
		string s = \
		"\n"
		"“Hey, what did I do to offend you?” you call down to the goat.\n"
		"“YE WERE STARING AT ME WHILE I DRANK MY MEAD!” the goat responds.\n"
		"“Well, I do apologize for offending you, sir!  I’ll be on my way, then, if\n"
		"that’s alright!”\n"
		"“AY, YOU DO THAT!  DON’T TRY MY PATIENCE, GNOME!” calls the goat.\n"
		"“I will make sure I don’t,” you mutter as you carry on up the mountain.\n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}

	void Ch3_Scene2::use()
	{
		typeText(protagonist->openInventory(), 50000);
		typeText("What item would you like to use?", 50000);
		string userChoice = protagonist->getUserItem();
		if(userChoice == "CLOSE")
		{
			stageValue = 2;
			completed = false;
			return;
		}

		while(!protagonist->checkInventory(userChoice))
		{
			typeText("You don't have any of those. Pick another item.\n", 50000);
			userChoice = protagonist->getUserItem();
		}
		if(userChoice == "MATCHES")
		{
			protagonist->removeItem("MATCHES", 1);
			string s = \
			"\n"
			"You strike a match and drop it down towards the goat, grinning like the\n"
			"pyromaniac that you are.  However, the wind catches the match and blows it into\n"
			"the mountainside, snuffing it out.  Sadness overwhelms you as you realize that\n"
			"you can’t light the goat on fire.  You crazy pyro. \n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "CRYSTAL BALL")
		{
			protagonist->removeItem("CRYSTAL BALL", 1);
			string s = \
			"\n"
			"You pull out the crystal ball and hurl it down at the goat. \n"
			"“That’ll teach you to mess with me!” you call down.\n"
			"“OH IT WILL, WILL IT?!?!” the goat bellows back. \n";
			typeText(s, 25000);
			stageValue = 3;
			completed = true;
		}
		if(userChoice == "PIXIE-B-GONE")
		{
			protagonist->removeItem("PIXIE-B-GONE", 1);
			string s = \
			"\n"
			"You spray the can of Pixie-B-Gone down at the goat.  However, the wind catches\n"
			"it and carries it off into the evening sky.  After emptying the can, you place\n"
			"it down on the ground next to you.  You then realize that the goat isn’t a\n"
			"pixie.  Whoops.  So hard to tell those two apart.\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "BERRY")
		{
			protagonist->removeItem("BERRY", 1);
			string s = \
			"\n"
			"You pull out one of the berries and eat it.  Its sweet, tangy juice fills your\n"
			"mouth, and you savour every last drop of flavour.  Then you remember that a\n"
			"mountain goat just flipped you off.  You should probably deal with that.\n";
			typeText(s, 25000);
			stageValue = 2;
			completed = false;
		}
		if(userChoice == "MOUTAIN MEAD BOTTLE")
		{
			protagonist->removeItem("MOUTAIN MEAD BOTTLE", 1);
			string s = \
			"\n"
			"You hold up the bottle of Mountain Mead so the goat can see it.  His eyes widen,\n"
			"and then he becomes enraged.  You swear that you can see a red tint to his fur.\n"
			"“YE DARE STEAL FROM ME, YE TREACHEROUS GNOME?!?!”\n"
			"Oh dear.  \n";
			typeText(s, 25000);
			stageValue = 3;
			completed = true;
		}				
	}

	void Ch3_Scene2::walk()
	{
		string s = \
		"\n"
		"“Well, I can’t fight him, and I don’t want to provoke him further,” you think to\n"
		"yourself, “I guess I’ll just carry on.”  With that, you turn around, decide to\n"
		"be the bigger man (metaphorically, of course), and walk away.  \n";
		typeText(s, 25000);
		stageValue = 0;
		completed = true;
	}
	