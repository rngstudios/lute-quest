//Ch4_Scene2.h

#ifndef CH4_SCENE2_H
#define CH4_SCENE2_H

#include "Player.h"
#include "PlayerChoiceStage.h"

class Ch4_Scene2 : public PlayerChoiceStage
{
public:


	Ch4_Scene2(Player * p);
	~Ch4_Scene2();
	bool chapterDone;

	void attack();
	void flee();
	void interact();
	void talk();
	void use();
	void walk();

private:
	Player * protagonist;
};
#endif