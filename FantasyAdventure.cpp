//FantasyAdventure.cpp

#include <iostream>
#include <string>
#include <fstream>
#include "GameHandler.h"
#include <ctime>

using namespace std;

int main(){
	clock_t startTime, endTime;
	startTime = time(NULL);

	GameHandler* gameHandler = new GameHandler(new TextAdventureGame());
	gameHandler->gameLoop();

	endTime = time(NULL);
	gameHandler->game->typeText(gameHandler->game->getTime(startTime, endTime), 40000);

	delete gameHandler;
	return 0;
}
