//Ch4_Scene3.h

#ifndef CH4_SCENE3_H
#define CH4_SCENE3_H

#include "Player.h"
#include "BinaryChoiceStage.h"

class Ch4_Scene3 : public BinaryChoiceStage
{
public:
	Ch4_Scene3(Player * p);
	~Ch4_Scene3();
	bool chapterDone;

	void getHelp();
	void choice_1();
	void choice_0();


private:
	Player * protagonist;
	int noCount;
};
#endif