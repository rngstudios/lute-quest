//Environment.cpp

#include "Environment.h"

Environment::~Environment(){}

///Calls the Stage method isComplete() on a specific stage within the vector 
///'stages' and returns the bool value of isComplete()
bool Environment::stageComplete(int stageNumber){
	return stages[stageNumber]->isCompleted();
}

bool Environment::environmentCompleted()
{
	return environmentComplete;
}

void Environment::runEnvironment(int stageIndex)
{
	printTitle();
	// -1 means the stage returned 0, so the environment is completed.
	while (!environmentComplete && !getActiveStage()->isGameSaved())
	{
		stageIndex = stages[stageIndex]->runStage();
		if(stageIndex == -1)
		{
			environmentComplete = true;
		} else if (stageIndex == 1337)
		{
			return;
		}
		//stageIndex++;
	}
}

void Environment::printTitle()
{
	std::cout << title << std::endl;
}


int Environment::getStartingStageIndex() {
	for (unsigned int i = 0; i < stages.size(); ++i)
	{
		if (!stageComplete(i))
		{
			return i;
		}
	}
	return stages.size() - 1;
}

Stage* Environment::getActiveStage() {
	return stages.at(getStartingStageIndex());
}

std::vector<Stage*> Environment::getStages() {
	return stages;
}

void Environment::setActiveStage(const int val) {
	for (int i = 0; i < val; i++) {
		stages.at(i)->setCompleted(true);
	}
}